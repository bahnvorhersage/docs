---
title:  Richtlinie zum "Software-Sprint"
subtitle: Bahn-Vorhersage
lang: de
geometry:
- top=2.5cm
- right=2.5cm
- left=2.5cm
- bottom=2cm
fontfamily: libertinus
fontsize: 11pt
colorlinks: true
numbersections: true
figureTitle: Abbildung
figPrefix: Abb.
tableTitle: Tabelle
tblPrefix: Tab.
secPrefix: Abschn.
author:
- "Zuwendungsempfänger: Theodor Döllmann"

# Citations
bibliography: bib.bib
---
<!-- Comdpile: pandoc --filter=pandoc-crossref --citeproc --pdf-engine=pdflatex -f markdown -t  pdf schlussbericht_prototypefund.md -o schlussbericht_prototypefund.pdf -->

-----

# Schlussbericht

Das diesem Bericht zugrundeliegende Vorhaben wurde mit Mitteln des Bundesministeriums für Bildung und Forschung unter dem Förderkennzeichen 01IS23S26 gefördert. Die Verantwortung für den Inhalt dieser Veröffentlichung liegt beim Autor.

# Kurze Darstellung der Aufgabenstellung und Motivation

Die fortschreitende Klimakrise stellt die Gesellschaft vor neue Herausforderungen. Besonders der Verkehrssektor hat laut Umweltbundesamt seit 1990 kaum Fortschritte im Klimaschutz erzielt. Daher ist hier der Handlungsdruck besonders groß. Für eine klimagerechte Verkehrswende braucht es eine leistungsstarke und zuverlässige Bahn. Auch bei großen Anstrengungen, die aktuell noch nicht getan werden, wird noch einige Zeit verstreichen, bis die deutsche Schieneninfrastruktur so ausgebaut ist, dass die Bahn das Rückgrat der klimaneutralen Mobilität darstellen kann. Um die Bahn dennoch jetzt attraktiver zu machen, benötigt es Zwischenlösungen, sodass mit Verspätungen besser umgegangen werden kann.

Für dieses Problem sollte im Rahmen des Projekts Bahn-Vorhersage ein Lösungsansatz erarbeitet werden: Bei der Suche nach Zugverbindungen sollen nicht nur die Zugverbindungen selbst angezeigt werden, sondern auch alternative Verbindungen von Unterwegshalten aus. Wird ein Anschlusszug verpasst, sind die Alternativen dadurch schon bekannt. So können Nutzer:innen das Verspätungsrisiko besser einschätzen - Zugverbindungen mit schlechten Alternativen bei Verspätungen können vermieden werden.

Das Projekt Bahn-Vorhersage besteht schon länger. Es beschäftigte sich bisher mit der Prognose der Anschlusssicherheit innerhalb von Zugverbindungen. Dafür existierte bereits eine Webseite und ein Datensammler für Fahrplan- und Echtzeitdaten der Bahn in Deutschland. Dieses bestehende System sollte nun mit der Erarbeitung der folgenden Meilensteine ergänzt werden, sodass es Alternativen mit anzeigen kann:

- Einbindung der DB `RIS::Stations` API, um dynamische Umsteigezeiten pro Bahnhof zu berechnen
- Umstrukturierung der Datenbasis auf ein routingfähiges Format (GTFS[^gtfs] ähnlich)
- Entwicklung eines Routing-Algorithmus mit Beachtung von Anschlussprognosen
- Einbindung von Echtzeitdaten (z.B. aktuelle Verspätungen) in das Routing
- Einbindung des neuen Algorithmus in die Webseite [bahnvorhersage.de](https://bahnvorhersage.de/), UI Design zur Darstellung der neuen Verbindungssuchen.

[^gtfs]: General Transit Feed Specification ([gtfs.org](https://gtfs.org/))

# Beitrag des Projektes zu den Zielen der Förderinitiative "Software-Sprint"

Zielgruppe sind alle Personen in Deutschland, die den ÖPNV nutzen oder gerne nutzen wollen. Aktuell müssen alternative Zugverbindungen an Umsteigebahnhöfen manuell herausgesucht werden. Durch das Projekt sollen diese schwierig erreichbaren Informationen zu Alternativen von Zugverbindungen übersichtlich dargestellt werden. Dadurch bekommen Nutzer:innen eine bessere und einfachere Möglichkeit, Zugreisen zu planen und mit erwartbaren Verspätungen umzugehen.

Dass Reisende einen besseren Zugang zu Reiseinformationen haben, führt zu besseren selbstbestimmten Entscheidungen und liegt daher im Einklang mit den Zielen der Förderinitiative "Software-Sprint".

# Ausführliche Darstellung der Ergebnisse und Umsetzung der Meilensteine

Im Folgenden wird die genaue Umsetzung der Meilensteine pro Meilenstein dargestellt.

## Einbindung der DB `RIS::Stations`[^ris-station-api] API

Umsteigezeiten an Bahnhöfen sind ein komplexes Problem, da die benötigte Umsteigezeit stark vom Aufbau des Bahnhofs, der Position der reisenden Personen im Zug sowie deren Ortskenntnisse und Laufgeschwindigkeit abhängt. Warum keine einfache Lösung existiert, zeigt @fig:transfers exemplarisch: Zum einen können Gleise nicht als Zahlen interpretiert werden, und zum anderen sind die Gleise an jedem Bahnhof unterschiedlich angeordnet. Im Beispiel in @fig:transfers liegen Gleis 1 und 3 am gleichen Bahnsteig, während Gleis 5 und 6 an unterschiedlichen Bahnsteigen liegen. Ein Umstieg von Gleis 2b zu Gleis 6 Nord beinhaltet zudem einen vollständigen Wechsel der Bahnsteigseite.

![Exemplarische mögliche Umstiege zwischen verschiedenen Bahnsteigen](img/umstiege.png){#fig:transfers width=60%}

Da die Bahn ihre eigenen Daten hierzu im konzerneigenen Open-Data-Portal veröffentlicht, sollten diese verwendet werden. Hierzu wurden die gesamten Umsteige-Daten mittels Python aus der API in eine neo4j Graphendatenbank exportiert. Diese umfasst insgesamt 30.000 Gleise und 150.000 Umsteigemöglichkeiten. Wegen vieler Anfragen an die API der Bahn musste hierzu ein extra Zugang (für Open-Source-Projekte kostenlos) beantragt werden.

Leider stellte sich dabei heraus, dass die Bahn nur an einigen wenigen Bahnhöfen tatsächliche Umsteigezeiten zwischen den Gleisen berechnet hat. Größtenteils werden als Fallback vertragliche Zahlen des europäischen Fahrplanzentrums und der Richtlinie 420 verwendet. Diese geben lediglich eine sehr großzügig gemessene Umsteigezeit pro Bahnhof an. Daher ist es aktuell nicht sinnvoll, diese Zahlen für ein Routing zu verwenden, da die Qualität zwischen den einzelnen Bahnhöfen zu stark variiert.

[^ris-station-api]: API des Reisenden Informationssystems (RIS) für Daten rund um Bahnhöfe: https://developers.deutschebahn.com/db-api-marketplace/apis/product/ris-stations


## Umstrukturierung der Datenbasis auf ein routingfähiges Format

Die Daten lagen im Projekt Bahnvorhersage bisher in folgenden Formaten vor:

- Rohdaten der Abfahrtsmonitore aller Bahnhöfe, aufgeteilt in Fahrpläne (1h Intervall) und Fahrplanänderungen (2min Intervall)
- Tabellarische Darstellung der Daten pro Halt, ohne Verbindung zwischen den Halten

Um auf den Daten Zugverbindungen zu finden, müssen diese möglichst eine Ordnung nach Zugrouten aufweisen. Zudem sollen die Projektdaten mittelfristig veröffentlicht werden, weshalb das neue Format möglichst bekannt und dokumentiert sein sollte. Da GTFS das einzige bekannte und gut dokumentierte Format für ÖPNV-Daten ist und es zudem nach Zugrouten geordnet werden kann, sollten die Daten des Projekts in dieses Format übertragen werden. Die Abweichungen vom Fahrplan in GTFS-RT[^gtfs-rt] werden durch eine einzige Protocoll-buffer[^protobuf]-Datei dargestellt, weshalb sich das GTFS-Format nur bedingt eignet, um Verspätungsdaten zu archivieren. Es wäre nicht sinnvoll, die Echtzeitinformationen einiger Jahre gemäß GTFS-RT in einer einzigen, Terabyte-großen, Datei zu speichern. Bei der Umstrukturierung der Daten wurde daher darauf geachtet, dass das GTFS gut in einzelne Tage unterteilbar ist, sodass später einzelne Tage mit einzelnen GTFS-RT Feeds exportiert werden können.

[^gtfs-rt]: Echtzeitinformationen für statische GTFS Feeds. RT steht für Realtime.

[^protobuf]: Datenformat zur Serialisierung, entwickelt von Google.

Beim Parsen in das GTFS-Format müssen neu geparste Datenpunkte in eine Postgresql-Datenbank eingefügt werden. Dabei kann es sein, dass die Daten bereits in der Datenbank gespeichert sind. Daher ist es unumgänglich, ein `UPSERT` Statement zu verwenden. Dieses ist im Vergleich zu einem `INSERT` ziemlich langsam, weshalb der Parser auch nach einiger Optimierung und externer Beratung ca. 48 h benötigt, um knapp 600 Mio. Datenpunkte zu parsen.

## Entwicklung eines Routing-Algorithmus mit Beachtung von Anschlussprognosen

Seitdem im April 2023 die Meilensteine formell eingereicht wurden, hatte sich an den tatsächlichen Zielen des Vorhabens und damit auch den Anforderungen an den Routing-Algorithmus einiges geändert. Der Routing-Algorithmus soll in der Lage sein, "gute" Verbindungen und Alternativen zu diesen Verbindungen zu finden. Nun ist die Bewertung, was eine "gute" Verbindung ist, sehr subjektiv. In einer Nutzer:innenumfrage haben Bahnfahrende folgende Kriterien angegeben, nach denen sie Zugverbindungen auswählen:

- Fahrzeit
- Anzahl der Umstiege
- Preis
- Bequemlichkeit der Züge
- Mit Deutschlandticket nutzbar
- Nicht zu lange und nicht zu kurze Umsteigezeiten
- Schönheit der gefahrenen Strecke
- Zuverlässigkeit der Verbindung

Nicht alle diese Kriterien sind algorithmisch abbildbar, und z.B. der Preis lässt sich DB extern nicht zuverlässig bestimmen. Dennoch ergibt sich, dass der Routing-Algorithmus verschiedene Kriterien abbilden muss (multi criteria), um für alle Bahnfahrenden passende Verbindungen zu finden. Zudem muss der Algorithmus dafür geeignet sein, Echtzeitdaten zu beachten.

In der Literatur gibt es verschiedene Ansätze zu Routing. Grundsätzlich unterscheiden sich diese von Routing-Algorithmen für nicht Fahrplan basierten Verkehr. Einige Beispiele sind:

- Graphen-basiert (Dijkstra)[^graph]
- RAPTOR[^raptor]
- Connection Scan[^csa]
- Transfer patterns[^transfer-patterns]

[^graph]: Fahrpläne werden in einen Graphen übertragen (häufige Beispiele: time-dependent Graph und time-extended Graph), wodurch der Dijkstra-Algorithmus darauf angewendet werden kann.

[^raptor]: Round-Based Public Transit Routing (RAPTOR): In Runden werden die gesamten Fahrverläufe einer Route betrachtet. Jede neue Runde entspricht einem weiteren Umstieg. Siehe @delling2015round.

[^csa]: Zugläufe werden in kleinste "Connections" zerteilt und nach Abfahrtszeit sortiert. Anschließend "Scannt" der Algorithmus ein mal über die sortierten Connections und kann so optimale Verbindungen finden. Siehe @dibbelt2017connection.

[^transfer-patterns]: Beschleunigungstechnik, die ausnutzt, dass die optimalen Verbindungen zwischen zwei Orten immer über dieselben Umsteigebahnhöfe führen. Diese werden im Voraus berechnet. Siehe @bast2010fast.

Graphenbasierte Algorithmen benötigen häufig eine starke Vorverarbeitung der Daten und sind daher eher schlecht für die Beachtung von Echtzeitdaten geeignet. Im ersten Test hat ein auf RAPTOR basierender Algorithmus sehr schlecht performt. Transfer Patterns schienen beim Totalausfall von Knotenpunkten und Fahrplanänderungen (wie z.B. immer wieder während des Baus von Stuttgart 21) unzuverlässig.

Daher wurde als Basis für den neuen Algorithmus der Connection Scan gewählt. Weil die restliche Codebase des Projekts in Python geschrieben ist, wurde für einen Prototyp des Routing-Algorithmus ebenso Python gewählt, obwohl dies eigentlich als interpretierte Sprache nicht für eine performante Routing Engine geeignet ist. Um die Leistung trotzt Python gewährleisten zu können, wurden die Datentypen des Routing-Algorithmus so gewählt, dass der Code mit dem Numba[^numba]-JIT[^jit] in leistungsstarken Machine Code compiliert werden kann.

[^numba]: Numba übersetzt Python-Funktionen in optimierten Machine Code mithilfe des LLVM-Compilers ([numba.pydata.org](https://numba.pydata.org))

[^jit]: Just in time Compiler

Da es mehr Anforderungen an den Routing-Algorithmus gibt, als für CSA bereits implementiert sind, wurde der Algorithmus um Folgendes erweitert:

- Das Finden von Pareto[^pareto]-Optimalen Verbindungen nach beliebig vielen Kriterien, nicht nur der Earliest Arrival Time und der Anzahl an Umstiegen
- Simulation einer möglichen Verspätung eines einzelnen Zuges
- Aussortierung von Verbindungen, die mehr als 30 km vom Ziel weg fahren (Beschleunigung)


[^pareto]: Eine Pareto-Optimale Zugverbindung ist eine Zugverbindung, bei der keine andere Zugverbindung existiert, die eine Eigenschaft (z.B. die Anzahl der Umstiege) verbessert, ohne eine andere Eigenschaft (z.B. die Fahrtdauer) zu verschlechtern.

Dies ermöglicht es in zwei Schritten, Verbindungen und deren Alternativen zu finden. Im ersten Schritt werden Pareto-optimale Verbindungen zwischen Start und Ziel nach den folgenden Kriterien gesucht:

- Ankunfts- und Abfahrtszeit
- Anzahl der Umstiege
- Gefahrene Streckenlänge
- Ausschließlich Regionalzüge
- Möglichst lange Umsteigezeiten

Anschließend werden für alle Umstiege in den Verbindungen Alternativen gesucht. Dabei wird davon ausgegangen, dass ein Zug maximal 30 Minuten verspätet sein kann. Alternativen werden ab dem Bahnhof vor dem beachteten Umstieg gesucht, da es möglich ist, im Voraus über Verspätungen Bescheid zu wissen. Bei der Alternativensuche wird simuliert, dass der ankommende Zug für einen Umstieg bis zu 30 Minuten verspätet ist. Es wird mindestens eine alternative Verbindung gesucht, die auch mit 30 Minuten Verspätung noch erreicht werden kann. Die Alternativverbindungen werden nach folgenden Kriterien Pareto-optimiert:

- Ankunftszeit
- Anzahl der Umstiege
- Ausschließlich Regionalzüge
- Ob die Verbindung am Umstiegsbahnhof startet
- Umsteigezeit von verspätetem Zug aus
- Möglichst lange Umsteigezeiten

Für die Pareto-Dominanz wird die von @schnee2009fully definierte relaxed Pareto-Dominanz verwendet. Diese macht es möglich, genauere Dominanz-Regeln zu definieren, um einzelne Faktoren zu gewichten. Dies wird genutzt, um z.B. die gefahrene Streckenlänge nicht zu sehr ins Gewicht zu nehmen, da es kaum Vorteile dadurch gibt, eine 100 Meter kürzere Fahrstrecke zu haben.

Als der Routing-Algorithmus funktionsfähig war, wurde versucht, die Laufzeit mittels Numba zu verkürzen. Dies hat für kleine Unterfunktionen geklappt, der Versuch, den ganzen Code damit zu beschleunigen, ist gescheitert. Der Code ist zwar theoretisch von Numba unterstützt, allerdings ist der Compilevorgang so langsam, dass es zu keinem Ergebnis geführt hat.

## Einbindung von Echtzeitdaten (z.B. aktuelle Verspätungen) in das Routing

Wegen Zeitmangel wurde dieser Meilenstein übersprungen. Um in einem Prototyp zu zeigen, wie das Suchen von Alternativverbindungen konzeptionell aussehen und funktionieren kann, ist die Integration von Echtzeitdaten nicht zwingend notwendig.

## Einbindung des neuen Algorithmus in die Webseite [bahnvorhersage.de](https://bahnvorhersage.de/), UI Design zur Darstellung der neuen Verbindungssuchen

Die bestehende Webseite [bahnvorhersage.de](https://bahnvorhersage.de/) sollte um eine neue Such- und Anzeigefunktion für Alternativverbindungen erweitert werden. Die Seite ist eine progressive Web-App und basiert auf VueJS[^vue]. Wegen der kompakten und übersichtlichen Verbindungsdarstellung der App "Öffi[^oeffi]" sollte die neue Darstellung daran orientiert werden. In einer Coaching-Session mit Superbloom sowie einer anschließenden Nutzer:innenbefragung hat es sich als sinnvoll herausgestellt, zu jeder Verbindung einzeln alle Alternativen dieser Verbindung anzuzeigen. Die Anzeige wurde mit D3.js[^d3] umgesetzt; das Scrolling innerhalb der Verbindungsanzeige basiert auf D3-zoom. D3.js stellt die gesamte Anzeige als SVG dar.

[^oeffi]: Open Source App für den öffentlichen Verkehr ([gitlab.com/oeffi/oeffi](https://gitlab.com/oeffi/oeffi)).

[^superbloom]: Design coaching für public interest, open-source, civic tech, nonprofit ([superbloomdesign.notion.site](https://superbloomdesign.notion.site/)).

[^vue]: JavaScript Frontend Framework ([vuejs.org](https://vuejs.org/)).

[^d3]: Data driven documents ist ein low level Toolbox zum Visualisieren von Daten ([d3js.org](https://d3js.org/)).

Für Nutzer:innen sieht die Benutzung der Seite wie folgt aus:

Auf der Seite [bahnvorhersage.de/routing](https://bahnvorhersage.de/routing/) können Nutzer:innen einen Start- und Zielbahnhof sowie die geplante Uhrzeit einer Reise angeben. Anschließend werden mögliche Zugverbindungen gesucht und wie in [@fig:result] dargestellt.

![Beispielhafte Zugverbindungen von Tübingen Hbf nach Augsburg Hbf](img/routing_funktionsweise.png){#fig:result width=60%}

Eine Zugverbindung besteht aus den breiten Rechtecken mit Zugnummern darauf und ist von Oben (Abfahrt) nach Unten (Ankunft) zu lesen. Die schmalen Balken rechts neben der Zugverbindung deuten die möglichen Alternativen an, wenn ein Anschluss verpasst wird. Sie sind mit einer Linie zu dem Punkt in der Zugverbindung verbunden, an dem die alternative Verbindung startet.

Um weitere Verbindungen zu sehen, kann die Darstellung nach Links "gewischt" werden bzw. können Pfeil-Tasten und Schaltflächen zur Navigation verwendet werden. Die Details von einzelnen Verbindungen werden durch Klicken auf die Verbindung angezeigt.


<!-- Die Meilensteine konnten bis auf die Einbindung von Echtzeitdaten in das Routing umgesetzt werden. Im Folgenden sind die Endergebnisse in die Abschnitte Nutzer:innenoberfläche und Backend unterteilt. @sec:frontend geht darauf ein, wie die Ergebnisse praktisch genutzt werden können; @sec:backend behandelt die technische Umsetzung.

## Nutzer:innenoberfläche {#sec:frontend}

Auf der Seite [bahnvorhersage.de/routing](https://bahnvorhersage.de/routing/) können Nutzer:innen einen Start- und Zielbahnhof sowie die geplante Uhrzeit einer Reise angeben. Anschließend werden mögliche Zugverbindungen gesucht und wie in [@fig:result] dargestellt.

![Beispielhafte Zugverbindungen von Tübingen Hbf nach Augsburg Hbf](img/routing_funktionsweise.png){#fig:result width=60%}

Eine Zugverbindung besteht aus den breiten Rechtecken mit Zugnummern darauf und ist von Oben (Abfahrt) nach Unten (Ankunft) zu lesen. Die schmalen Balken rechts neben der Zugverbindung deuten die möglichen Alternativen an, wenn ein Anschluss verpasst wird. Sie sind mit einer Linie zu dem Punkt in der Zugverbindung verbunden, an dem die alternative Verbindung startet.

Um weitere Verbindungen zu sehen, kann die Darstellung nach Links "gewischt" werden bzw. können Pfeil-Tasten und Schaltflächen zur Navigation verwendet werden. Die Details von einzelnen Verbindungen werden durch Klicken auf die Verbindung angezeigt.

Die Webseite ist zudem als Progressive Web-App verfügbar, kann damit auf Android-Smartphones installiert werden und bietet dadurch eine App-ähnliche Bedienung.

## Backend {#sec:backend}

Die schon bestehenden Daten des Projekts Bahn-Vorhersage wurden in das routingfähige Format GTFS[^gtfs] umgewandelt. Neue Daten werden in Echtzeit in dieses Format übertragen.

Auf diesen Daten kann eine Python-Implementation des Connection-Scan-Algorithmus (CSA) alle Pareto-Optimalen[^pareto] Zugverbindungen nach folgenden Kriterien finden:

- Ankunfts- und Abfahrtszeit
- Anzahl der Umstiege
- Gefahrene Streckenlänge
- Ausschließlich Regionalzüge
- Möglichst lange Umsteigezeiten

Nachdem die Zugverbindungen gefunden wurden, kann der CSA zu diesen Verbindungen Alternativen finden. Dabei wird davon ausgegangen, dass ein Zug maximal 30 Minuten Verspätung erreichen kann. -->


# Zielgruppe, Nutzen und mögliche Weiterentwicklungen

Durch das Vorschlagen von Alternativen für Zugverbindungen können Nutzer:innen besser einschätzen, wie zuverlässig ihre Zugverbindungen bei Verspätungen sind. Es ist möglich einfach zu sehen, ob bei einem verpassten Anschluss mit insgesamt z.B. 10, 30, 60 oder 120 Minuten Verspätung gerechnet werden muss. Dadurch, dass das Projekt Open Source ist, kann das Routing vollständig nachvollzogen und in anderen Projekten genutzt werden. Dies könnte besonders interessant für Forschungen auf dem Gebiet des öffentlichen Verkehrs sein.

Da die zugrunde liegenden Daten jetzt in einem öffentlich dokumentierten Dateiformat vorliegen, ist eine eventuelle zukünftige Veröffentlichung einfacher. Dies kann aber erst nach der Klärung einiger rechtlicher Fragen geschehen.

Die neu entwickelte Routing Engine soll auf Dauer das gesamte Routing von Verbindungen im Projekt Bahn-Vorhersage übernehmen. Dafür muss allerdings die Geschwindigkeit verbessert werden. Vermutlich würde es genügen, die Routing Engine in einer schnellen, compilierten Programmiersprache wie Rust neu zu schreiben. Dies würde es auch ermöglichen, Verspätungsvorhersagen und Anschlussprognosen einzubauen.

# Kurze Darstellung der Arbeiten, die zu keiner Lösung geführt haben

Der letztendlich verwendete, CSA basierte, Routingalgorithmus ist nicht der erste Entwurf gewesen. Im Voraus wurde schon ein Graphen-basierter Ansatz und ein RAPTOR-basierter Ansatz entwickelt, die aber beide nicht die nötige Geschwindigkeit aufweisen konnten und daher jeweils verworfen wurden.

Zudem wurden die Ergebnisse des ersten Meilensteins nicht mit eingebaut, da sich die von der `RIS::Stations` API herausgegebenen Umsteigezeiten als häufig unrealistisch lange herausstellten. Daher wurde zur Entwicklung des Routingalgorithmus eine konstante Umsteigezeit angenommen. Auf Dauer werden hier aber vermutlich dynamische Umsteigezeiten mit einfließen. 

# Kurze Angabe von Präsentationsmöglichkeiten für mögliche Nutzer

Im Folgenden sind die verschiedenen Seiten aufgelistet, die weitere Informationen zu dem Projekt enthalten.

**Projektwebseite:** [bahnvorhersage.de](https://bahnvorhersage.de/)

**Neues Routing mit Alternativvorschlägen:** [bahnvorhersage.de/routing](https://bahnvorhersage.de/routing/)

**Source Code auf GitLab:** [gitlab.com/bahnvorhersage](https://gitlab.com/bahnvorhersage/)

# Kurze Erläuterung zur Einhaltung der Arbeits- und Kostenplanung

Die Entwicklung des Routingalgorithmus hat deutlich mehr Zeit in Anspruch genommen als erwartet, daher wurde die Integration von Echtzeitdaten ausgelassen, um dennoch einen funktionierenden Prototyp präsentieren zu können.

# Kurze Darstellung von etwaigen Ergebnissen bei anderen Stellen

Es gibt einige weitere Projekte, die an ÖPNV-Routing arbeiten. Zu nennen sind besonders Motis ([motis-project.de](https://motis-project.de/)) und ein Projekt von Robin Durner ([traines.eu](https://traines.eu/)), der an einem auf Wahrschleinlichkeitsverteilungen basierendem Routing arbeitet. In Zukunft könnte eine Integration zwischen den Tools interessant werden. Für die Entwicklung eines Prototyps, der zeigt, wie eine Fahrplanauskunft mit Alternativvorschlägen funktionieren könnte, war dies noch nicht sinnvoll. Dennoch gab es einen Austausch zwischen den Projekten.

\newpage{}

# Literaturverzeichnis

::: {.references}
:::