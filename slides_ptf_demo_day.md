---
marp: true
theme: default
class: invert
math: katex
---

# <!-- fit --> Bahn-Vorhersage

## Alternative Zugverbindungen mitplanen

<!--
Hi, ich bin Theo und arbeite seit über vier Jahren an dem Projekt Bahn-Vorhersage. Heute werde ich präsentieren, was ich im Rahmen des Prototypefunds an meinem Projekt erarbeitet habe
-->

---

# Challenge

<!--
Als erstes möchte ich dabei meine Motivation vorstellen. Ich möchte meine Zukunft gerne so mitgestallten, dass diese Lebenswerter ist als heute. Aktuell sehen ich durch die fortschreitende Klimakrise eher das Gegenteil.
-->

---

![bg](<img/Erderwärmung ein Jahr über 1,5 Grad.png>)

<!--
2023 lag die Durchschnittstemperatur erstmals durchgehend 1.5°C über dem langjährigen mittel. Auf Dauer führt eine Erderwärmung zur Häufung von Stürmen, Hochwasser sowie Hitze und Dürreperioden. Das wiederum kann zu Wasser und lebensmittelknappheit, Massenfluchtbewegungen und Ressourcenkriegen führen. Also zu allgemein Katastrophalen Zuständen. Daher sollten wir unsere Anstrengungen erhöhen, dies zu verhindern.
-->

---

Während die Treibhausgasemissionen in Deutschland seit 1990 stark gesunken sind, gab es im **Verkehrssektor bisher kaum eine Verbesserung**. Der Anteil des Verkehrs an den Gesamtemissionen ist seit 1990 von 13 % auf 19 % im Jahr 2021 gestiegen.
 
</br>
Umweltbundesamt 2023

<!--
Der aktuelle Stand im Deutschen Verkehrssektor ist dabei laut Umweltbundesamt wie folgt: ...

Fazit: Hier ist handlungsbedarf! Ein Hinderungsgrund der Treibhausgasreduzierung ist die marode Bahn.
-->

---

# Wie kann eine bessere Bahn und die Verkehrswende noch gelingen?

* **Politik:** mehr Geld investieren und Strukturen verbessern
* **Bürger:innen:** Druck für Verkehrswende erhöhen

<!--
Hauptverantwortlich für diesen Zustand ist die Politik, die mehr Geld in die Schiene investieren muss, aber auch wir sollten wie heute am Deutschlandweiten Klimastreik den Druck auf die Politik erhöhen.

Eine Folgerung daraus ist aber, dass diese Verbesserungen ihre Zeit brauchen, und wir Erstmal mit den Zuständen umgehen müssen.

Ich habe ein kleines Beispiel mitgebracht um darauf über zu leiten, was genau ich jetzt gemacht habe:
-->

---

# Zugreise zu wichtigem Termin

![bg left:30%](img/db_navigator_ticket_buchen_fertig_los.png)

<!--
Ich möchte mit dem Zug zu einem wichtigen Termin. Mit dem DB-Navigator soll das einfach sein: "Ticket buchen, fertig, los!"
-->

---

# Ups, Zug verpasst

![bg left:30%](<img/train missed.jpeg>)

<!--
Etwas später stehe ich am Gleis und habe meinen Anschlusszug verpasst. Den Termin erreiche ich vielleicht nicht mehr, weshalb ich sofort zurück nach Hause fahre.
-->

---

# Nächste Planung:

![bg right:40% contain](img/db_navigator_collage.png)

<!--
Beim nächsten mal wird meine Planung nun nicht mehr "Ticket buchen fertig los" sein. Ich muss mir verschiedene Verbindungen genau anschauen, und werde versuchen Alternativen mit zu planen, indem ich von jedem Umsteigebahnhof aus eine eigene Verbindungssuche starte. Das ist nervig, unübersichtlich und zeitaufwändig.
-->

---

# Wie kann mehr Vertrauen in den ÖPNV geschaffen werden?

➜ Mit Alternativvorschlägen bei der Routensuche

<!--
Wie könnte mehr Vertrauen geschaffen werden?

Indem grundsätzlich bei der Planung auch Alternativen mitbetrachtet werden, denn wenn ich mit Verspätungen und alternativen plane, wirken diese nicht wie der Ausnahmefall, sondern gehören von Anfang an zu meiner Planung dazu.
-->

---

# <!-- fit --> Bahn-Vorhersage

# <!-- fit --> Routing

<!--
Und genau das habe ich nun umgesetzt. Bevor wir uns aber ansehen, wie das genau tut, möchte ich kurz noch eine ganz Zentrale Frage für dieses Vorhaben zwischenschieben.
-->

---

# Was macht eine "gute" Zugverbindung aus?
* Kurze Fahrzeit
* Wenige Umstiege
* Mit Deutschlandticket fahrbar
* Passende Umsteigezeiten
* Bequemlichkeit der Züge
* Preis

<!--
Was macht eigentlich eine Gute Zugverbindung aus? Wir machen das jetzt kurz ganz interaktiv. Ich ein paar mögliche Kriterien vorlesen, und ihr meldet euch jeweils, wenn das eurer Meinung nach was ausmacht.

...

Was wir hier sehen, ist das die Frage für unterschiedliche Menschen unterschiedlich beantwortet werden muss, was es besonders schwierig macht, diese Kriterien einem Computer beizubringen.

-->

---

# Wie kann ich das jetzt nutzen?

<!--
Jetzt kommen wir aber endlich dazu, wie ihr das jetzt nutzen könnt. Dafür gehen wir auf die Webseite des Projekts dessen link ich später noch eingebaut habe.
-->

---

![bg contain](img/warning.png)

<!--
Da ignorieren wir als aller erstes die große Rote warnung, die uns sagt was alles noch nicht funktioniert und warum wir es lieber doch nicht nutzen sollten. In der Förderung war leider nur ein Prototyp enthalten ;)
-->

---

![bg width:721px](img/routing_search.png)

<!--
Da kriegen wir dann einfach eine eingabemaske, und können Dort Start, Ziel und Uhrzeit eingeben.

Ich fahre ab zwischen Tübingen und Augsburg, daher habe ich das einfach mal eingegeben. (ist falsch hier)
-->

---

![bg contain](<img/routing result.png>)

<!--
Das Ergebnis bekommen wir dann so präsentiert. Das ist sicherlich erst mal etwas gewöhnungsbedürftig, daher erkläre ich das kurz:

Eine Zugverbindung sind immer die fetten Balken von oben nach unten. Nebendran sind jeweilige Alternativen angedeutet.

Wenn ich bei der ersten Verbindung zum Beispiel den Anschluss verpassen sollte, sehe ich dass ca 15 min bzw eine Stunde später ein weiterer Zug fährt. Wenn ich den Anschluss knapp verpassen sollte, wäre es also nicht so schlimm
-->

---

# <!-- fit --> https://bahnvorhersage.de/routing

**Vorsicht:** Es ist wirklich nur ein Prototype

<!--
Das wars jetzt auch schon mit dem Vortrag. Hier gibt es den Link. In Zukunft werde ich vermutlich wieder unbezahlt an dem Projekt arbeiten, bis ich mir eine neue Förderung suche. Da ist aber vielleicht schon was angedacht.
-->