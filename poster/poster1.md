---
marp: true
theme: poster
paginate: false
size: 33:46
---

<!-- Start header -->
<div class="header">

<!-- Image in the upper left -->
<div>

![height:8cm headerlogo](img/logo_jugend-forscht_wir-foerdern-talente.svg)

</div>

<!-- Title and author information -->
<div>

# Bahn-Vorhersage

## https://bahnvorhersage.de

## Theo Döllmann

</div>

<!-- Image on the upper right -->
<div style="display: flex; justify-content: space-around;">

![height:8cm headerlogo](img/IC.png)

</div>

<!-- End header -->
</div>

<!-- Start main 2 column split for poster -->
<div class="columns-main">

<!-- Start main column 1 -->
<div>

### Datenbasis
#### Zuglaufinformationen
- Knapp 8 Mio. Anfragen pro Tag
- Knapp 500 Mio. Datenpunkte (1,2 TB)

<hr>

![Alt text](img/uebersicht_datenparsing.png)
**Abb 1:** Ablauf Datenparsing
<hr>

#### Stationsdaten und Streckendaten
- Sehr viele verschiedene Datenquellen
- Keine Datenquelle annähernd vollständig
- Viele Besonderheiten (Doppelte Bahnhofsnamen etc.)

<hr>

![Alt text](plots/streckennetz.png)
**Abb. 2:** Streckennetz gesammelt von © OpenStreetMaps
<hr>

### Infrastruktur

![Alt text](img/tech_stack.png)

**Abb. 3:** Tech Stack

<hr>

<!-- End main column 1 -->
</div>

<!-- Start main column 2 -->
<div>

### Auswertung der Daten

#### Verschiedene Zugtypen
- Besonders der Fernverkehr ist verspätet
- Besonders der Fern- und Busverkehr fällt aus

<hr>
<div class=columns2>
<div>

![center height:15.5cm](plots/cancellations_per_train_type.png)
**Abb. 4A:** Zugausfälle pro Zugtyp

</div>
<div>

![center height:15.5cm](plots/delay_per_train_type.png)
**Abb. 4B:** Zugverspätungen pro Zugtyp


</div>
</div>
<hr>

#### Verspätungsverteilung
- Die meisten Züge sind pünktlich
- Hohe Verspätungen sind extreme Ausreißer
- Züge mit mehr Verspätung fallen öfter aus.
<hr>
<div class=columns2>
<div>

![Alt text](plots/per_delay.png)
**Abb. 5A:** Lineare Skala

</div>
<div>

![Alt text](plots/per_delay_loggy.png)
**Abb. 5B:** Logarithmische Skala


</div>
</div>
<hr>

#### Pünktlichkeit im Tages- und Stundenverlauf
- Zum Berufsverkehr fahren mehr Züge
- Wenn viele Züge fahren, sinkt die Pünktlichkeit
- Zur vollen und halben Stunde halten weniger Züge

<hr>

<div class="columns2">

<div>

![Alt text](plots/over_day_on_time_percentage.png)
**Abb. 6A:** Pünktlichkeit Tagesverlauf

</div>

<div>

![Alt text](plots/over_hour_on_time_percentage.png)
**Abb. 6B:** Pünktlichkeit Stundenverlauf

</div>

</div>

<hr>

#### 9€-Ticket
- Während des 9€-Tickets kam es vermehrt zu Verspätungen
- Verspätungen kommen nicht zwangsweise vom 9€-Ticket, da es im Sommer mehr Gleisarbeiten gibt

<hr>

<div class="columns2">

<div>

![Alt text](plots/neun_euro_vorher_light.webp)
**Abb. 7A:** Verspätungen vor 9€-Ticket

</div>

<div>

![Alt text](plots/neun_euro_light.webp)
**Abb. 7B:** Verspätungen während 9€-Tickets

</div>

</div>

<hr>

<!-- End main column 2 -->
</div>
<!-- End main columns -->
</div>


