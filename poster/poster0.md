---
marp: true
theme: poster
paginate: false
size: 33:46
---

<!-- Start header -->
<div class="header">

<!-- Image in the upper left -->
<div>

![height:8cm headerlogo](img/logo_jugend-forscht_wir-foerdern-talente.svg)

</div>

<!-- Title and author information -->
<div>

# Bahn-Vorhersage

## https://bahnvorhersage.de

## Theo Döllmann

</div>

<!-- Image on the upper right -->
<div style="display: flex; justify-content: space-around;">

![height:8cm headerlogo](img/IC.png)

</div>

<!-- End header -->
</div>

<div>

<br><br><br>

# Plane jetzt Deine Rückreise:

<br><br>

![center width:20cm](img/bahnvorhersage_qr_code_rueckfahrt_bremen.svg)
## bahnvorhersage.de

<br><br>

# Finde die zuverlässigste Verbindung


</div>

<br><br>

<div style="border: solid #224466 1cm; border-radius: 3cm;">

<h1 style="color: #224466 !important;" > Zahlen & Fakten: </h1>

<div class="columns-main" style="max-width: 60cm; margin: 3cm auto; gap:4cm;">

<div>

![center width:13cm](img/chart.svg)

## 500 Mio.
## Datenpunkte

</div>

<div>

![center width:13cm](img/geo-alt.svg)

## 11.000
## Bahnhöfe

</div>

<div>

![center width:13cm](img/database.svg)

## 1,2 TB
## Datenbank

</div>

</div>
<div class="columns-main" style="max-width: 60cm; margin: 3cm auto; gap:4cm;">

<div>

![center width:13cm](img/clock.svg)

## 4 Jahre
## Entwicklung

</div>

<div>

![center width:13cm](img/gitlab.svg)

## Open Source

</div>

<div>

![center width:13cm](img/train-front.svg)

## Gesamtes
## Bahnnetz

</div>

</div>

</div>

<!-- Start main 2 column split for poster -->
<div class="columns-main">

<!-- Start main column 1 -->
<div>

<!-- End main column 1 -->
</div>

<!-- Start main column 2 -->
<div>


<!-- End main column 2 -->
</div>
<!-- End main columns -->
</div>


