---
marp: true
theme: poster
paginate: false
size: 24:36
---

<!-- Start header -->
<div>

<!-- Title and author information -->
# Bahn-Vorhersage

## https://bahnvorhersage.de/

## Theo Döllmann

<!-- End header -->
</div>

<!-- Start main 2 column split for poster -->
<div class="columns-main">

<!-- Start main column 1 -->
<div>

### Datenbasis
#### Zuglaufinformationen
- Knapp 8 Mio. Anfragen pro Tag
- Gut 1.2 Mrd. Datenpunkte
- Der Nahverkehr ist sehr gut abgebildet
- Der Fernverkehr wird pünktlicher wahrgenommen

<hr>

![alt text](../plots/over_year_monthly.png)
**Abb 1:** Monatliche Zugverspätungen

<hr>

#### Stationsdaten und Streckendaten
- Sehr viele verschiedene Datenquellen
- Keine Datenquelle annähernd vollständig
- Viele Besonderheiten (Doppelte Bahnhofsnamen etc.)

<hr>

![Alt text](../plots/streckennetz.png)
**Abb. 2:** Streckennetz von © OpenStreetMaps

<hr>

#### Anzahl gesammelte Daten über die Zeit
- Daten Seit Oktober 2021
- Verspätungsentwicklung seit Oktober 2023
- Kleine Aussetzer wegen Serverschäden

<hr>

![alt text](../plots/over_year_weekly_count.png)
**Abb: 3** Gesammelte Datenpunkte über die Zeit

<hr>

<!-- End main column 1 -->
</div>

<!-- Start main column 2 -->
<div>

### Auswertung der Daten

#### Verspätungen pro Zugtyp

- Besonders der Fernverkehr ist verspätet
- Auch einige Regios haben deutliche Verspätungen

<hr>

![height:10cm center](../plots/per_train_type_delay.png)
**Abb. 4:** Verspätung pro Zugtyp

<hr>

#### Ausfälle

- Besonders der Fern- und Busverkehr fällt aus

<hr>
<div class=columns2>
<div>

![height:10cm](../plots/per_train_type_cancellation.png)
**Abb. 5A:** Langfristig

</div>
<div>

![height:10cm](../plots/per_train_type_spontaneous_cancellation.png)
**Abb. 5B:** Kurzfristig (<2h)


</div>
</div>
<hr>

#### Verspätungsverteilung
- Hohe Verspätungen sind extreme Ausreißer
- Züge mit mehr Verspätung fallen öfter aus
<hr>

![width:17cm center](../plots/per_delay.png)
**Abb. 6:** Verteilung von Verspätungen und Zugausfällen

<hr>

#### Pünktlichkeit im Tages- und Stundenverlauf
- Zum Berufsverkehr fahren mehr Züge
- Wenn viele Züge fahren, sinkt die Pünktlichkeit
- Zur vollen und halben Stunde halten weniger Züge

<hr>

<div class="columns2">

<div>

![alt text](../plots/over_day.png)
**Abb. 7A:** Pünktlichkeit Tagesverlauf

</div>

<div>

![alt text](../plots/over_hour.png)
**Abb. 7B:** Pünktlichkeit Stundenverlauf

</div>

</div>

<hr>



<!-- End main column 2 -->
</div>
<!-- End main columns -->
</div>


