---
marp: true
theme: poster
paginate: false
size: 24:36
math: katex
---

<!-- Start header -->
<div>

<!-- Title and author information -->
# Bahn-Vorhersage

## https://bahnvorhersage.de/

## Theo Döllmann

<!-- End header -->
</div>

<!-- Start main 2 column split for poster -->
<div class="columns-main">

<!-- Start main column 1 -->
<div>

#### 9€-Ticket
- Währendessen kam es vermehrt zu Verspätungen
- Verspätungen kommen nicht zwangsweise vom 9€-Ticket, im Sommer wird auch mehr gebaut

<hr>

<div class="columns2">

<div>

![Alt text](../plots/neun_euro_vorher_light.webp)
**Abb. 8A:** Verspätungen vor 9€-Ticket

</div>

<div>

![Alt text](../plots/neun_euro_light.webp)
**Abb. 8B:** Verspätungen während 9€-Ticket

</div>

</div>

<hr>

### Genauigkeit der Vorhersagen

<div style="border: .1cm solid black; border-radius: 1cm; padding: 0.5cm; margin: 0.5cm 2cm">

<h4 style="text-align: center"> Prognosemodelle: </h4>

$$
\textrm{Ankunft:} \textrm{Ankunftsverspätung} \leq n \in \{0, 1, ..., 14 \textrm{ Minuten}\}
$$
$$
\textrm{Abfahrt:} \textrm{Abfahrtsverspätung} \geq n \in \{0, 1, ..., 14 \textrm{ Minuten}\}
$$
</div>

- Modelle für niedrige Verspätungen schneiden gut ab
- Modelle für höhere Verspätungen schneiden nicht ganz so gut ab

<hr>
<div class=columns2>
<div>

![Alt text](../plots/pred_accu_ar_overview.png)
**Abb. 9A:** Modelle für die Ankunft

</div>
<div>

![Alt text](../plots/pred_accu_dp_overview.png)
**Abb. 9B:** Modelle für die Abfahrt


</div>
</div>

<hr>

### Modellvorhersagen über den Tag
- Die zentralen Tendenzen der Pünktlichkeit bildet das Modell sehr gut ab

<hr>
<div class=columns2>
<div>

![Alt text](../plots/over_day_on_time_2022_09_01.png)
**Abb. 10A:** Tatsächliche Pünktlichkeit

</div>
<div>

![Alt text](../plots/over_day_predictions_2022_09_01.png)
**Abb. 10B:** Prognostizierte Pünktlichkeit


</div>
</div>

<hr>

<!-- End main column 1 -->
</div>

<!-- Start main column 2 -->
<div>

### Verspätungen im Streckenverlauf

- Mit zunehmender gefahrener Strecke nimmt die Verspätung von Zügen zu
- Über die Fahrstrecke summieren sich viele Verspätungsfaktoren

<hr>
<div class="columns2">

<div>

![alt text](../plots/over_distance/delay_ICE.png)
**Abb 11A:** Verspätung ICE

</div>

<div>

![alt text](../plots/over_distance/delay_RE.png)
**Abb 11B:** Verspätung RE

</div>
</div>
<hr>

### Auswertung Verbindungsscore

#### Fünf Minuten Umsteigezeit
- Der Verbindungsscore kann Umstiege mit gleicher geplanter Umsteigezeit ranken
- Die Korrelation zwischen Verbindungsscore und tatsächlicher Umsteigezeit ist auch per Rangkorrelationskoeffizient nach Spearman (Korrelation: $0.1$, P-Wert: $3 \cdot 10^{-15}$) nachweisbar

<hr>
<div class=columns2>
<div>

![Alt text](../plots/con_score_5_min_planned_transfer_time.png)
**Abb. 12A:** Geplante

</div>
<div>

![Alt text](../plots/con_score_5_min_real_transfer_time.png)
**Abb. 12B:** Tatsächlich

</div>
</div>
<hr>

#### Zwei bis zehn Minuten Umsteigezeit
- Realistisches SzenarioN
- Der Verbindungsscore ist maßgeblich abhängig von der planmäßigen Umsteigezeit
- Weil die Regressionsgerade in 11B steiler verläuft als in 11A, bildet der Verbindungsscore die tatsächliche Verspätung besser ab als die Umsteigezeit nach Fahrplan

<hr>
<div class=columns2>
<div>

![Alt text](../plots/con_score_2_to_10_min_planned_transfer_time.png)
**Abb. 13A:** Geplant

$$ y = \boldsymbol{5.2} \cdot x + 2.2 $$

</div>
<div>

![Alt text](../plots/con_score_2_to_10_min_real_transfer_time.png)
**Abb. 13B:** Tatsächlich

$$ y = \boldsymbol{8.4} \cdot x - 0.1 $$

</div>
</div>
<hr>

<!-- ### Umsteigezeiten
- Gleisnamen sind nicht nur Nummern
- Kein Bahnhof ist gleich

<hr>

![width:20cm center](img/umstiege.png)

**Abb. 12:** Symbolische Darstellung verschiedener Umsteigemöglichkeiten

<hr> -->


### Gefördert von:

<div class=row>

![height:5cm](img/logo-bmbf.svg)

![height:5cm](img/ptf-logo.png)

![height:5cm](img/sfz_logo.png)

</div>


<!-- End main column 2 -->
</div>


<!-- End main columns -->
</div>
