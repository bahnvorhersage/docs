---
marp: true
theme: poster
paginate: false
size: 33:46
math: katex
---

<!-- Start header -->
<div class="header">

<!-- Image in the upper left -->
<div>

![height:8cm headerlogo](img/logo_jugend-forscht_wir-foerdern-talente.svg)

</div>

<!-- Title and author information -->
<div>

# Bahn-Vorhersage

## https://bahnvorhersage.de

## Theo Döllmann

</div>

<!-- Image on the upper right -->
<div style="display: flex; justify-content: space-around;">

![height:8cm headerlogo](img/IC.png)

</div>

<!-- End header -->
</div>

<!-- Start main 2 column split for poster -->
<div class="columns-main">

<!-- Start main column 1 -->
<div>

### Genauigkeit der Verspätungsvorhersagen

<div style="border: .1cm solid black; border-radius: 1cm; padding: 0.5cm; margin: 0.5cm 2cm">

<h4 style="text-align: center"> Prognosemodelle: </h4>

$$
\textrm{Ankunft:} \textrm{Ankunftsverspätung} \leq n \in \{0, 1, ..., 14 \textrm{ Minuten}\}
$$
$$
\textrm{Abfahrt:} \textrm{Abfahrtsverspätung} \geq n \in \{0, 1, ..., 14 \textrm{ Minuten}\}
$$
</div>

- Modelle für niedrige Verspätungen schneiden gut ab
- Modelle für höhere Verspätungen schneiden nicht ganz so gut ab

<hr>
<div class=columns2>
<div>

![Alt text](plots/pred_accu_ar_overview.png)
**Abb. 8A:** Modelle für die Ankunft

</div>
<div>

![Alt text](plots/pred_accu_dp_overview.png)
**Abb. 8B:** Modelle für die Abfahrt


</div>
</div>

<hr>

### Modellvorhersagen über den Tag
- Die zentralen Tendenzen der Pünktlichkeit bildet das Modell sehr gut ab

<hr>
<div class=columns2>
<div>

![Alt text](plots/over_day_on_time_2022_09_01.png)
**Abb. 9A:** Tatsächliche Pünktlichkeit

</div>
<div>

![Alt text](plots/over_day_predictions_2022_09_01.png)
**Abb. 9B:** Prognostizierte Pünktlichkeit


</div>
</div>

<hr>

### Auswertung Verbindungsscore

#### Verbindungsscore fünf Minuten Umsteigezeit
- Der Verbindungsscore kann Umstiege mit gleicher geplanter Umsteigezeit ranken
- Die Korrelation zwischen Verbindungsscore und tatsächlicher Umsteigezeit ist auch per Rangkorrelationskoeffizient nach Spearman (Korrelation: $0.1$, P-Wert: $3 \cdot 10^{-15}$) nachweisbar

<hr>
<div class=columns2>
<div>

![Alt text](plots/con_score_5_min_planned_transfer_time.png)
**Abb. 10A:** Geplante Umsteigezeit

</div>
<div>

![Alt text](plots/con_score_5_min_real_transfer_time.png)
**Abb. 10B:** Tatsächliche Umsteigezeit

</div>
</div>
<hr>

#### Verbindungsscore zwei bis zehn Minuten Umsteigezeit
- Realistisches SzenarioN
- Der Verbindungsscore ist maßgeblich abhängig von der planmäßigen Umsteigezeit
- Weil die Regressionsgerade in 11B steiler verläuft als in 11A, bildet der Verbindungsscore die tatsächliche Verspätung besser ab als die Umsteigezeit nach Fahrplan

<hr>
<div class=columns2>
<div>

![Alt text](plots/con_score_2_to_10_min_planned_transfer_time.png)
**Abb. 11A:** Geplante Umsteigezeit

$$ y = \boldsymbol{5.2} \cdot x + 2.2 $$

</div>
<div>

![Alt text](plots/con_score_2_to_10_min_real_transfer_time.png)
**Abb. 11B:** Tatsächliche Umsteigezeit

$$ y = \boldsymbol{8.4} \cdot x - 0.1 $$

</div>
</div>
<hr>

<!-- End main column 1 -->
</div>

<!-- Start main column 2 -->
<div>

### Umsteigezeiten
- Gleisnamen sind nicht nur Nummern
- Kein Bahnhof ist gleich

<hr>

![width:20cm center](img/umstiege.png)

**Abb. 12:** Symbolische Darstellung verschiedener Umsteigemöglichkeiten

<hr>

#### Umsteigegraph Bremen Hbf

- 25 verschiedene Gleise, Busbahnhöfe und Abschnitte
- 338 verschiedene mögliche Umsteigemöglichkeiten
- Insgesammt 30.000 Gleise und 150.000 Umsteigemöglichkeiten

<hr>

![width:40cm center](img/transfers%20bremen_no_background.png)

**Abb. 13:** Umsteigemöglichkeiten Bremen Hbf

<hr>


### Unterstützungsleistungen
Hier könnte Ihr Logo stehen. Im Ernst: Dieses Big Data Projekt ist sehr ressourcenintensiv, und ich suche händeringend nach weiteren Sponsoren. 

Ich danke allen, die diese Arbeit ermöglicht und das Projekt unterstützt haben:
- SFZ Eningen
- Bosch
- Herr Mörike
- Förderkreis des Kepler-Gymnasiums Tübingen
- Micron
- Uni Tübingen
- Felix Engelmann
- Jochen Völker
- Prof. Dr. Rolf Moeckel


![width:400px center](img/sfz_logo.png)

<!-- End main column 2 -->
</div>


<!-- End main columns -->
</div>
