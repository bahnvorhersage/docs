---
marp: true
class: invert
---

# <!-- fit --> Bahn-Vorhersage

---


# Wer bin ich

* Theo Döllmann
* Verkehrswende- & Klimaaktivist
* Uni Augsburg
* Geoinformatik
* info@bahnvorhersage.de

![bg right:40% auto](img/theo.jpg)

---

# Gefördert von:

![height:200px](img/logos/bmbf_logo.svg) ![height:200px](img/logos/prototypefund_logo.svg) ![height:200px](img/logos/sfz_logo.svg)


---

# Agenda

* Motivation
* Showcase
* Technische Umsetzung (Vorhersagen)
* Fragen?
<!-- * Datenherkunft
* Neu: Routing von Alternativen
* Ein paar Datenspielereien -->

---

# <!-- fit --> Motivation

---

# Zugreise zu wichtigem Termin

![bg left:30%](img/db_navigator_ticket_buchen_fertig_los.png)

<!--
Ich möchte mit dem Zug zu einem wichtigen Termin. Mit dem DB-Navigator soll das einfach sein: "Ticket buchen, fertig, los!"
-->

---

# Ups, Zug verpasst

![bg left:30%](<img/train missed.jpeg>)

<!--
Etwas später stehe ich am Gleis und habe meinen Anschlusszug verpasst. Den Termin erreiche ich vielleicht nicht mehr, weshalb ich sofort zurück nach Hause fahre.
-->

---

# Nächste Planung:

![bg right:40% contain](img/db_navigator_collage.png)

<!--
Beim nächsten mal wird meine Planung nun nicht mehr "Ticket buchen fertig los" sein. Ich muss mir verschiedene Verbindungen genau anschauen, und werde versuchen Alternativen mit zu planen, indem ich von jedem Umsteigebahnhof aus eine eigene Verbindungssuche starte. Das ist nervig, unübersichtlich und zeitaufwändig.
-->

---

# Wie kann mehr Vertrauen in den ÖPNV geschaffen werden?

* Erhöhung der Pünktlichkeit
* *Hahaha*
* **Bessere Informationen für Reisende**
  * Qualitätsvorhersagen
  * Bekannt schlechte Verbindungen meiden

---

# <!-- fit --> Showcase

---

![bg contain](<img/Verbindungssuche Bahn-Vorhersage.png>)

---

![bg contain](img/Verbindung.png)

---

# <!-- fit --> Technische Umsetzung

---

# Wie Vorhersagen?

* Vorhersage von Verspätungen
* XGBoost *(eXtreme Gradient Boosting)*
* Verrechnung zur Zuverlässigkeit 

![bg right contain](<img/Delay prediction 10 classes.png>)

---

### Wie gut sind die Vorhersagen?

Übersicht

![bg right:67% contain invert](plots/pred_accu_ar_overview.png)

---

### Wie gut sind die Vorhersagen?

Tagesverlauf Beobachtung

![bg right:67% contain invert](plots/over_day_on_time_2022_09_01.png)



---

### Wie gut sind die Vorhersagen?

Tagesverlauf Vorhersagen

![bg right:67% contain invert](plots/over_day_predictions_2022_09_01.png)

---

### Wie gut ist der Verbindungsscore?

![bg right:64% contain invert](plots/con_score_2_to_10_min_planned_transfer_time.png)

---

### Wie gut ist der Verbindungsscore?

![bg right:64% contain invert](plots/con_score_2_to_10_min_real_transfer_time.png)

---

### Verwendete Merkmale

![bg right:67% contain invert](plots/feature_importance_ar.png)

---

# <!-- fit --> Fragen?

<!-- ---

# Datensammeln à la David Kriesel, nur

* mehr
* öfters
* länger

![bg right](<img/david kriesel.webp>)

---

# Datenbasis

* 9000 Bahnhöfe
* 1,2 TB Rohdaten
* 650 Mio Zughalte


![bg right contain](img/iris_tafel.png)

---

# Was, wenn die Verbindungssuche Alternativen zu verpassbaren Anschlüssen anzeigt?

---

![bg contain](img/routing_funktionsweise.png)

---

![bg contain](img/routing_köln_sbahn.png)

---

<!-- # fit --> 
<!-- Datenspielerei

--- -->

<!-- #_class: -->

<!-- # 9€ Ticket

Vorher

![bg right contain](plots/neun_euro_vorher_light.webp)

---

<!-- _class: -->

<!-- # 9€ Ticket

Währenddessen

![bg right contain](plots/neun_euro_light.webp) -->