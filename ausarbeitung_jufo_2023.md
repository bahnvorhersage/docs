---
title: Bahn-Vorhersage
author: Theo Döllmann
date: "11. März 2023"
geometry: top=2.5cm, bottom=2cm, left=2.5cm, right=2.5cm
numbersections: true
toc: true
toc-title: Inhaltsverzeichnis
colorlinks: true
listings: True

figureTitle: Abbildung
figPrefix: Abb.

tableTitle: Tabelle
tblPrefix: Tab.

secPrefix: Abschn.

# eisvogel template
titlepage: true
titlepage-rule-height: 8
titlepage-logo: img/IC.png
toc-own-page: true
---

<!-- 
compile to pdf:
pandoc -H head.tex -V lang=de --pdf-engine=xelatex --template eisvogel --filter pandoc-crossref --listings -s -o Langfassung_Bahnvorhersage_2023.pdf README.md

compress pdf to 300 dpi:
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=Langfassung_Bahnvorhersage_2023_300_DPI.pdf Langfassung_Bahnvorhersage_2023.pdf
-->

\pagenumbering{roman}

\newcommand\vheader{-2ex}


\vspace{\vheader}
# Abstract {.unnumbered .unlisted}
\vspace{\vheader}


In dreieinhalb Jahren Arbeit konnte ich mit meinem Big-Data-Projekt Bahn-Vorhersage zeigen, dass es durchaus möglich ist, die Anschlusssicherheit von Zugverbindungen mehrere Tage im Voraus vorherzusagen. Durch eine aufwendige Datenfusion aus historischen Zugläufen, Bahnhofsinformationen und Daten des europäischen Schienennetzes habe ich einen Datensatz mit über 470 Millionen Datenpunkten erstellt. Mit diesem Datensatz, der nur mit Big Data Tools verarbeitet werden kann, habe ich einem XGBoost Machine Learning Modell beigebracht, Zugverspätungen vorherzusagen. Mit diesen Vorhersagen kann ich auf meiner Webseite [bahnvorhersage.de](https://bahnvorhersage.de/) die Anschlusssicherheit von zukünftigen Zugverbindungen vorhersagen. Ich konnte nachweisen, dass es einen positiven Zusammenhang zwischen den von mir erstellten Anschlussprognosen und den tatsächlichen Umsteigezeiten gibt.

Wegen großer Verspätungen ist der öffentliche Verkehr in Deutschland für viele Menschen unattraktiv. Mit meinem Projekt werde ich die Zuverlässigkeit der Bahn nicht steigern können (dafür bräuchte es vor allem politischen Willen und massive Investitionen in die Schieneninfrastruktur), allerdings können Nutzer:innen meiner Webseite besonders störungsanfällige Zugverbindungen gezielt vermeiden.

\vspace{\vheader}
# Open Source {.unnumbered .unlisted}
\vspace{\vheader}

Das Projekt ist Open Source. Der gesamte Quellcode ist auf meiner Webseite verlinkt: [bahnvorhersage.de/opensource](https://bahnvorhersage.de/opensource#content)

\vspace{\vheader}
# Webseite {.unnumbered .unlisted}
\vspace{\vheader}

Die Webseite zu dem Projekt: [https://bahnvorhersage.de/](https://bahnvorhersage.de/)

\newpage{}

\pagenumbering{arabic}

\vspace{\vheader}
# Einleitung {#sec:intro}
\vspace{\vheader}

Vor fast vier Jahren, ursprünglich für den _Bundeswettbewerb Künstliche Intelligenz_ 2019[^bwki], haben mein damaliger Projektpartner Marius und ich beschlossen, Zugverspätungen mithilfe von Künstlicher Intelligenz vorherzusagen.

[^bwki]: 2019 haben Marius und ich mit diesem Projekt den Wettbewerb gewonnen: [https://www.bw-ki.de/rueckblick#c94](https://www.bw-ki.de/rueckblick#c94)

Auf meiner Webseite [https://bahnvorhersage.de/](https://bahnvorhersage.de/) kann man Zugverbindungen suchen und finden, ähnlich wie auf der Seite der Bahn. Zusätzlich zu den bekannten Verbindungsdetails der Bahn gibt meine Webseite für jede Verbindung einen "Verbindungsscore" aus. Dieser beschreibt die Wahrscheinlichkeit, alle Anschlusszüge zu erreichen.

Dafür sammle ich seit zweieinhalb Jahren rund um die Uhr einen riesigen, terrabytegroßen Datensatz an Verspätungsdaten aus ganz Deutschland und darüber hinaus. Diese verschneide ich mit Informationen über Bahnhöfe und das europäische Schienennetz. Ein maschinelles Lernverfahren nutzt diese Daten, um aus vergangenen Ereignissen die Zugverspätungen der nächsten Wochen vorherzusagen.

Die besondere Schwierigkeit bei diesem Projekt liegt in der Datenbasis und der Größe des Datensatzes. Hinzu kommt, dass sich Zugverspätungen chaotisch entwickeln, was eine Vorhersage extrem schwierig macht. Denn die Ursachen für Verspätungen sind vielfältig: Personalmangel, Bäume auf den Gleisen oder defekte Fahrzeuge. All diese Gründe sind nicht vorhersagbar. Das hat zur Folge, dass sich auch die Verspätungen selbst fast unberechenbar entwickeln.

Die eigentlichen Ursachen für Zugverspätungen liegen in den PKW-fixierten Entscheidungen der letzten 20 Jahre, die maßgeblich von CDU/CSU/FDP geprägt wurden. Der Schiene fehlt vor allem eines: Streckenkapazität. Viele Bahnstrecken sind zu über 100 Prozent ausgelastet, was zu Staus und Verspätungen führt. Die Züge haben dann keine Chance mehr, eine Verspätung wieder aufzuholen.

\vspace{\vheader}
# Datenquellen
\vspace{\vheader}

Um ein System zur Vorhersage von Zugverbindungen zu entwickeln, benötige ich historische Echtzeitdaten der Ankunfts- und Abfahrtszeiten von Zügen über einen längeren Zeitraum. Außerdem benötige ich möglichst reichhaltige Daten über die verschiedenen Faktoren, die zu Verspätungen führen, damit das maschinelle Lernverfahren diese bei der Vorhersage von Verspätungen berücksichtigen kann. Die Daten stellten sich als das Hauptproblem des Projekts heraus. Vergangene Verspätungsdaten sind in Deutschland nicht verfügbar. Informationen zur Schieneninfrastruktur, die einen wesentlichen Einfluss auf Verspätungen haben, sind immer nur für Teile der Infrastruktur mit großen Datenlücken und geringer Qualität veröffentlicht[^data_sources_strecke].

[^data_sources_strecke]: Die Datenquellen sind in [@sec:streckendaten] aufgelistet

\vspace{\vheader}
## Historische Zugläufe
\vspace{\vheader}

Die Suche nach einer geeigneten Datenquelle für vergangene Zugverspätungen erwies sich als äußerst schwierig. Die einzige Lösung für Bahnexterne scheint zu sein, die Daten live zu erfassen. Dafür gibt es verschiedene mögliche Schnittstellen der Bahn:

- HAFAS[^hafas]
- IRIS[^iris]
- EFA[^efa]
  
Der HAFAS-Endpoint hat allerdings ein zu niedriges Rate Limit. Da ich für die EFA keine Dokumentation finden konnte, habe ich mich für das IRIS entschieden. Bei diesem muss jede Bahnhofstafel von jedem Bahnhof mindestens alle 2 Minuten einzeln abgefragt werden. Letztendlich mache ich es recht ähnlich, wie David Kriesel es in einem Vortrag "BahnMining - Pünktlichkeit ist eine Zier"[^bahn_mining] zeigt, nur hatte ich in meiner Arbeit Jugend Forscht (2021, Kapitel 3) festgestellt, dass Herr Kriesels Daten fehlerbehaftet sind[^davids_fehler]. Deshalb frage ich die Livedaten der Bahnhöfe nicht wie er stündlich, sondern alle zwei Minuten ab, da die Daten nach Abfahrt eines Zuges gelöscht werden. Außerdem frage ich 9238 Bahnhöfe ab, also ca. 30-mal mehr als Herr Kriesel. Das macht:

[^hafas]: Das HaCon **Fa**hrplan-**A**uskunfts-**S**ystem (HAFAS) ist eine Software für die Fahrplanauskunft des Unternehmens HaCon (Hannover Consulting). HAFAS steckt z.B. hinter [https://bahn.de/](https://bahn.de/) und dem DB-Navigator. 

[^iris]: Das **I**nterne-**R**eisenden-**I**nformations**S**ystem (kurz IRIS) versorgt Bahnhofstafeln mit Daten, wie z.B. diese Tübinger Abfahrtstafel: [https://iris.noncd.db.de/wbt/js/index.html?typ=ab&bhf=TT](https://iris.noncd.db.de/wbt/js/index.html?typ=ab&bhf=TT)

[^efa]: Die **E**lektronische **F**ahrplan**a**uskunft (EFA) ist ein Softwareprodukt der Firma MENTZ GmbH. Z. B., hier verwendet: [http://www.efa-bw.de/](http://www.efa-bw.de/)

[^bahn_mining]: Vortrag von David Kriesel über die Pünktlichkeit der Bahn: [https://media.ccc.de/v/36c3-10652-bahnmining_-_punktlichkeit_ist_eine_zier](https://media.ccc.de/v/36c3-10652-bahnmining_-_punktlichkeit_ist_eine_zier)

[^davids_fehler]: In der Zwischenzeit habe ich mit David Kriesel persönlich darüber gesprochen und ihm meine Auswertungen gezeigt. Er hatte sich diesen Aspekt nie angeschaut.


$$
9238 \cdot \frac{60 \cdot 24}{2} = 6.651.360 \space \frac{\textrm{Anfragen}}{\textrm{Tag}}
$$

Das Kontingent der IRIS-API im Bahn-API-Marketplace[^6] ist dafür zu begrenzt. Ich verwende daher eine zweite, öffentliche Schnittstelle[^7] des internen Reisendeninformationssystems.

[^6]: Informationen zu Ankünften und Abfahrten in Form von Gleistafeln und Informationen zu einer Fahrt eines Zuges mittels DB Timetables API [https://developers.deutschebahn.com/db-api-marketplace/apis/product/timetables](https://developers.deutschebahn.com/db-api-marketplace/apis/product/timetables)

[^7]: Es wundert mich, dass noch keiner der Betreiber (DB Station & Service AG) der API bei mir nachgefragt hat, was ich da mache. Ich hatte das Vorgehen zwar angemeldet, allerdings nie eine Antwort darauf bekommen.

Trotz großem Aufwand sind die Daten der IRIS-API mangelhaft. Sie sind nur auf die Minute genau und zeigen häufig an, dass ein Zug deutlich zu früh ankommt oder abfährt. Zumindest letzteres kommt bei der Bahn in der Realität nicht vor. Die Daten habe ich in der Arbeit Jugend Forscht (Kapitel 3, 2021) gründlich analysiert und stütze mich hier teilweise auf die Erkenntnisse von damals.

\vspace{\vheader}
## Eisenbahninfrastrukturdaten
\vspace{\vheader}

Der Hauptgrund für Verspätungen ist die mangelnde Kapazität des Schienennetzes. Dies führt zu "Staus". Für die Prognose von Zugverspätungen sind daher Informationen über Bahnhöfe und das Streckennetz von besonderer Bedeutung.

\vspace{\vheader}
### Stationsdaten
\vspace{\vheader}

Es gibt viele verschiedene Datenquellen für Stationsdaten. Keine dieser Quellen ist vollständig. Das IRIS hat keine Geo-Koordinaten, die private Sammlung von "derf"[^derf] mit Koordinaten ist unvollständig. Das Zentrale Haltestellenverzeichnis (ZHV) verwendet andere IDs als die Bahn. Ähnlich verhält es sich mit HAFAS, Marudor und dem OpenData Bahnhofssatz der Bahn. Ich kombiniere alle diese Daten je nach Verfügbarkeit und habe auch viele Bahnhöfe manuell bearbeitet. Außerdem sind die Bahnhöfe nicht statisch, sondern ändern sich (Name, Lage, Existenz). In dieser einfach erscheinenden Grundlage stecken deshalb über 300 h Arbeit. Wegen des großen Aufwandes veröffentliche ich den Datensatz für andere auf meiner Homepage [https://bahnvorhersage.de/stationviewer](https://bahnvorhersage.de/stationviewer)

[^derf]: Perl-Kommandozeilen-Client für den DB IRIS Abfahrtsmonitor [https://github.com/derf/Travel-Status-DE-IRIS](https://github.com/derf/Travel-Status-DE-IRIS)

\vspace{\vheader}
### Notiz zu Stationen, Stationsnamen, EVAs[^eva] und der DS100[^ds100]
\vspace{\vheader}

Jeder Bahnhof hat einen Namen, eine EVA ID und eine DS100. Für Abfragen an einige APIs werden Namen, für andere EVAs (z.B.: Hafas Routing) und für wieder andere DS100-Abkürzungen (z.B.: IRIS) verwendet, um die Stationen zu identifizieren.

Namen sind keine eindeutigen Schlüssel. Die Straßenbahnhaltestellen in Karlsruhe sind oft in zwei Haltestellen aufgeteilt. Gleis 1 hat den gleichen Namen wie Gleis 2, aber andere Koordinaten und eine andere EVA.

[^eva]: Eine Art Haltestellennummer
[^ds100]: Druckschrift 100, ein Abkürzungsverzeichnis für Bahnhöfe und Betriebsstellen der Deutsche Bundesbahn (eigentlich veraltet, neu wäre die Ril100).

\vspace{\vheader}
### Streckendaten {#sec:streckendaten}
\vspace{\vheader}

Besonders entscheidend für die Pünktlichkeit eines Zuges ist die bereits zurückgelegte Strecke in Metern. Je weiter ein Zug fährt, desto mehr zufällige Ereignisse können zu Verspätungen führen. Da die tatsächlich gefahrene Strecke oft ein Vielfaches der Luftlinie beträgt, ist ein routingfähiges Streckennetz notwendig, um genau nachvollziehen zu können, wie weit ein Zug gefahren ist.
Das Streckennetz ist noch komplizierter als die Bahnhofsdaten: In etwa einem halben Jahr Arbeit habe ich mich durch sechs verschiedene Datenquellen (OpenData-Portal[^geo_strecke_db_opendata], Trassenfinder API[^trassenfinder] [^hm], DELFI[^delfi] GTFS[^gtfs] / NeTEx[^netex], Eisenbahnbundesamt über das Informationsfreiheitsgesetz[^ifg], DB-Netze und das Infrastrukturregister[^isr]) gearbeitet, um am Ende festzustellen, dass keine dieser Quellen vollständig ist. Meist ist nur der Teil des Streckennetzes enthalten, der von DB-Netze betrieben wird. Oder bei DELFI fehlten z.B. Informationen zu Sachsen und Schleswig-Holstein.

[^geo_strecke_db_opendata]: Geoinformationen zu Strecken des Schienenverkehrsnetzes: [https://data.deutschebahn.com/dataset/geo-strecke.html](https://data.deutschebahn.com/dataset/geo-strecke.html)

[^trassenfinder]: Auskunftssystem für eine Routensuche auf dem deutschen Schienennetz: [https://trassenfinder.de/](https://trassenfinder.de/)

[^hm]: Abstände in Hektometern (1 hm = 100 m) - eine meiner Meinung nach etwas willkürlich gewählte Einheit.

[^delfi]: Die **D**urchgängige **el**ektronische **F**ahrplan**i**nformation (DELFI) bietet eine deutschlandweite Verbindungsauskunft im öffentlichen Verkehr: [https://www.delfi.de/](https://www.delfi.de/)

[^gtfs]: Die **G**eneral **T**ransit **F**eed **S**pecification (GTFS) (Früher Google Transit Feed Specification) definiert ein digitales Austauschformat für Fahrpläne des öffentlichen Personenverkehrs und dazugehörige geografische Informationen. [https://developers.google.com/transit/gtfs](https://developers.google.com/transit/gtfs)

[^netex]: **Ne**twork and **T**imetable **Ex**change: NeTEx ist der künftige europäische Standard für die Übertragung von Soll-Daten bezüglich Netz und Fahrplan.

[^ifg]: FragDenStaat ist die zentrale Anlaufstelle für Informationsfreiheit in Deutschland: [https://fragdenstaat.de/](https://fragdenstaat.de/)

[^isr]: Infrastrukturregister von DB Netze [https://geovdbn.deutschebahn.com/isr](https://geovdbn.deutschebahn.com/isr)

Deshalb erstelle ich mein Streckennetz aus OpenStreetMaps-Daten. Diese müssen in einen routingfähigen Graphen umgewandelt werden. Dazu müssen die Bahnhöfe mithilfe eines eigens entwickelten Algorithmus als Knoten in den Schienengraphen eingefügt werden. Der Algorithmus konstruiert auf jedem Bahnhof ein Linienkreuz und fügt die Schnittpunkte des Kreuzes mit den Schienen als Bahnhofsknoten in den Graphen ein. Der grobe Ablauf ist derselbe wie in Jugend Forscht (2021, Abschnitt 2.3). Allerdings wurde der Code komplett neu geschrieben, um mit größeren Datenmengen und mehr Sonderfällen umgehen zu können. Die Arbeit mit Geodaten stellt eine besondere Herausforderung dar. Viele Frameworks sind unfertig, stürzen häufig ab und sind oft schlecht dokumentiert. Der Detaillierungsgrad des so erzeugten Streckennetzes ist sehr hoch: Jede einzelne Weiche ist abgebildet. Das führt dazu, dass ich Probleme habe, das gesamte Streckennetz in den Arbeitsspeicher zu laden und zu bearbeiten.

\vspace{\vheader}
# Datenverarbeitung
\vspace{\vheader}

Die gesammelten Daten über die Verspätungen können nicht in ihrer Rohform verwendet werden. Aufgrund der enormen Datenmengen muss die Verarbeitung der Daten gut durchdacht sein. Viele Verarbeitungsschritte, die normalerweise bei einem Datensatz trivial sind, können bei Big Data nicht angewendet werden.

In [@fig:data_parsing] ist der grobe Ablauf der Datenverarbeitung von Verspätungsdaten dargestellt. In diesem Fall fehlen die Datenquellen und das Parsing der Stations- und Streckendaten, die nochmal deutlich komplexer sind als das gesamte Diagramm hier.

![Übersicht über das Parsing](img/uebersicht_datenparsing.png){#fig:data_parsing width=100%}

Im Folgenden beschreibe ich einige Aspekte des Parsings genauer.

\vspace{\vheader}
## IDs
\vspace{\vheader}

Die planmäßigen Abfahrtszeiten und die geänderten Abfahrtszeiten werden von IRIS getrennt zur Verfügung gestellt. Damit diese miteinander kombiniert werden können, benötigt jeder Datenpunkt eine eindeutige ID. Das IRIS verwendet Zeichenketten als IDs. Eine beispielhafte ID _-406941045342705729-2009042251-1_ besteht aus folgenden Teilinformationen: _Täglich wiederverwendete ID der Zugline_ `-` _Datum_ `-` _Haltnummer_. Für die Verarbeitung mit Dask[^dask] empfehlen sich Integers als IDs[^dask_int_ids]. Daher werden die IRIS IDs mithilfe von CityHash[^cityhash] in 64-Bit Integer gehasht. Übrigens ist zumindest das Dask SQL Modul auch mit Integer IDs noch sehr störungsanfällig. Mit 5 eingearbeiteten Pull Requests für Dask SQL bin ich mittlerweile einer der Haupt-Mitwirker des Moduls. Diese Pull-Requests (s. @sec:opensource) entstanden alle aus akuten Problemen beim Herunterladen meiner Daten.

[^dask]: Big Data Framework für Python: [dask.org](https://www.dask.org/)

[^dask_int_ids]: Die SQL-Schnittstelle von Dask funktioniert nicht richtig mit Zeichenketten-IDs, und grundsätzlich machen Integer IDs Dask deutlich performanter.

[^cityhash]: Non Cryptographic Hash von Google: [github.com/google/cityhash](https://github.com/google/cityhash)

\vspace{\vheader}
## Echtzeit-Parsing
\vspace{\vheader}

Die Daten sollen möglichst in Echtzeit fertig geparst in der Datenbank abgelegt werden. Ein Datencrawler sollte die Daten möglichst in Rohform abspeichern. So können die Daten erneut geparst werden, wenn ein Fehler auffällt. Also müssen Datencrawler und Parser getrennt sein. Die Datencrawler speichern die IDs von neu gesammelten Daten in einem Redis Stream[^redis_stream] ab. Ähnlich wie in der Pub/Sub Architektur greift der Parser diese IDs ab, sobald sie veröffentlicht werden, und parst die entsprechenden Datenpunkte.

[^redis_stream]: Append only Datenstruktur der In-Memory Datenbank Redis. [https://redis.io/](https://redis.io/)

\vspace{\vheader}
## Routing und Performance
\vspace{\vheader}

Das informativste Merkmal für die Verspätungsvorhersage mittels Machine Learning ist die Länge der bereits gefahrenen Strecke. Diese berechne ich durch ein Routing (Dijkstra) in meinem Streckennetz. Das ist sehr rechenaufwendig. Die Rohdaten werden wegen der Größe in Chunks von je 20.000 Datenpunkten verarbeitet. Das Routing für 20.000 Datenpunkte dauert 14 Minuten. Für alle aktuell 470 Mio. Datenpunkte macht das ca. 237 Tage, also fast ein ganzes Jahr. Das Routing selber ist kaum weiter optimierbar: Ich verwende die in C geschriebene Bibliothek Igraph, und im Streckennetzgraphen habe ich alle unnötigen Kanten und Knoten entfernt oder zusammengefasst.

Die Routings entlang der Zugläufe im Voraus zu berechnen ist nicht sinnvoll, da fast täglich ein Zug so umgeleitet wird, wie davor noch nie ein anderer Zug gefahren ist. Daher ist das Routing während des Parsings zwingend notwendig.

Zur Beschleunigung verwendet der Parser unter anderem an dieser Stelle Caching, bestehend aus einem lokalen Cache und einen externen, geteilten Cache. Nur bei zwei Cache Misses wird die Routenlänge tatsächlich berechnet. Der geteilte Cache liegt in einer HashMap in einer Redis Datenbank. Dieser ist dafür notwendig, dass verschiedene Prozesse gleichzeitig parsen können und durch den geteilten Cache das Routing nicht doppelt ausführen müssen.

\vspace{\vheader}
## Geparste Daten
\vspace{\vheader}

Die Daten aus dem IRIS nach dem Parsen sind in [@tbl:iris_data] beispielhaft dargestellt. Zudem ist beschrieben, warum ich diese Merkmale für signifikant für die Verspätungsvorhersage halte.

|        Bahnhof        | Betreiber |  Typ  | Nummer | Verspätung (An) | Verspätung (Ab) | Gleis | Halt  | Minute |  Tag  | Aufenthaltszeit |
| :-------------------: | :-------: | :---: | :----: | :-------------: | :-------------: | :---: | :---: | :----: | :---: | :-------------: |
|    Bruxelles-Nord     |    88     |  IC   |  2112  |        0        |        0        |   7   |   3   |  762   |   2   |       2.0       |
|         Kaub          |    0S     |   S   | 47203  |       -1        |        0        |   2   |  18   |  589   |   1   |       0.0       |
| Friedrichshafen Stadt |  800694   |  RE   |  4217  |        0        |        0        |   4   |  13   |  863   |   6   |       5.0       |
| Nürnberg-Dutzendteich |  800721   |   S   | 39397  |        2        |        3        |   2   |   4   |   82   |   6   |       0.0       |
|     Teutschenthal     |    AM     |  Bus  | 10775  |        0        |        0        |       |   7   |  741   |   6   |       0.0       |
|     Ludwigsfelde      |  800158   |  RE   |  3305  |        1        |        1        |   1   |  28   |  600   |   5   |       1.0       |

: Geparste Daten aus dem IRIS {#tbl:iris_data}

Bahnhof, Nominal
: Der Name des Bahnhofs. Züge in z.B. Köln Hbf sind überdurchschnittlich verspätet.

Betreiber, Nominal
: Die ID des Betreibers des Zugs. Unterschiedliche Betreiber fahren unterschiedlich pünktlich.

Typ, Nominal
: Der Zugtyp. Der Nahverkehr ist in der Regel deutlich pünktlicher als der Fernverkehr.

Nummer, Nominal
: Zugnummer

Verspätung (An), Metrisch, Optional
: Verspätung in Minuten bei der Ankunft des Zuges. Negative Zahlen bedeuten, dass der Zug zu früh angekommen ist.

Verspätung (Ab), Metrisch, Optional
: Verspätung in Minuten bei der Abfahrt des Zuges.

Gleis, Nominal, Optional
: Das Gleis (z.B. 1, 12, oder 3a-f). Bei Doppelbelegungen von Gleisen kommt es oft zu Verspätungen.

Halt, Metrisch
: Der wievielte Halt des Zuges ist dieser Halt? Je weiter ein Zug fährt, desto mehr Verspätung sammelt er.

Minute, Metrisch
: Die Minuten seit Tagesbeginn bis zur geplanten Ankunftszeit. Im Berufsverkehr fahren mehr Züge, und es kommt zu zusätzlichen Verspätungen.

Tag, Metrisch
: Tag der Woche (0-6). Am Wochenende ist das Bahngeschehen anders als an Werktagen.

Aufenthaltszeit, Metrisch, Optional
: Die geplante Aufenthaltszeit im Bahnhof. Viel Aufenthaltszeit wird oft dafür verwendet, um Verspätungen aufzuholen.


Darüber hinaus gibt es vier weitere Spalten für die geplante und die tatsächliche Ankunftszeit des Zuges sowie für die geplante und die tatsächliche Abfahrtszeit des Zuges. Diese werden nicht direkt für das Training der maschinellen Lernmodelle verwendet, sind aber für viele Analysen und das Filtern der Daten nach Datum sehr wichtig. Aus diesen Spalten werden unter anderem die Verspätungen berechnet.

Durch die Verschneidung der IRIS-Daten mit den Bahnhofs- und Streckendaten können die Daten um [@tbl:geo_data] ergänzt werden.

| Längengrad | Breitengrad | Distanz seit Startbahnhof | Distanz bis Endbahnhof |
| :--------: | :---------: | :-----------------------: | :--------------------: |
| 50.860239  |  4.361458   |         16892.367         |       16980.846        |
| 50.083652  |  7.768262   |         902847.56         |       839542.56        |
| 47.653166  |  9.473471   |         326894.0          |        38347.37        |
| 49.436959  |  11.117391  |         5633.5557         |       11794.492        |
| 51.464817  |  11.786027  |         16029.182         |       1051.8007        |
| 52.299214  |  13.267602  |         114937.91         |       47421.707        |

: Zusätzliche Geodaten aus dem Streckennetz {#tbl:geo_data}

Längengrad, Intervall
: Längengrad des Bahnhofs. Die Qualität des Schienennetzes variiert regional erheblich.

Breitengrad, Intervall
: Breitengrad des Bahnhofs. Die Qualität des Schienennetzes variiert regional erheblich.

Distanz seit Startbahnhof, Metrisch
: Gefahrene Strecke seit Startbahnhof des Zuges in Metern. Je weiter ein Zug fährt, desto mehr Verspätung sammelt er.

Distanz bis Endbahnhof, Metrisch
: Noch zu fahrende Strecke bis zum Endbahnhof des Zuges in Metern. Der letzte Halt fällt öfter aus als andere.

Die Daten aus [@tbl:iris_data] und [@tbl:geo_data] sind alle Merkmale, die für das Training der Machine Learning Modellen verwendet werden.

\vspace{\vheader}
# Big Data
\vspace{\vheader}

In dem Projekt arbeite ich mit Big Data. Also Daten, die so strukturiert (Rohdaten kommen als XML), schnell (alle 2 Minuten kommen neue Daten, die das Bahngeschehen in Echtzeit beschreiben) und groß (1.3 TB Datenbank) sind, dass ich sie nicht mehr mit herkömmlichen Verarbeitungsmethoden verarbeiten kann. Hier folgen einige Beispiele aus meinem Projekt, bei denen die Verarbeitung von Big Data besonders schwierig war.

\vspace{\vheader}
## Datenaggregierung für Plots
\vspace{\vheader}

Um im Feature Engineering reichhaltige Merkmale für die Verspätungsvorhersage zu entwickeln, benötigte ich unter anderem Visualisierungen meiner Daten. Zudem sind viele Blicke in die Daten des Bahngeschehens spannend. In der Arbeit Jugend Forscht (2021, Kapitel 3) hatte ich so Ausfälle bei der NordBahn unabsichtlich aufgedeckt und die Resultate eines Schneesturms analysiert. Für die meisten Analysen möchte ich alle Daten in Betracht ziehen. Das macht aus einem Einzeiler für normale Daten ein ganzes Modul für Big Data.

Die Daten müssen aggregiert werden. Das mache ich mit Dask[^dask]. Dask teilt die Daten in Blöcke, sogenannte Partitionen, auf. Für die Aggregierung werden die Partitionen einzeln aggregiert. Nur das Ergebnis der Berechnung wird im Arbeitsspeicher behalten. Alle Ergebnisse werden wiederum zusammengefasst. Auf den einzelnen Partitionen kann so parallel auf mehreren Prozessorkernen gearbeitet werden. Bei aktuell gut 2000 Partitionen ist das ganze dennoch langsam. Daher cache ich die Ergebnisse zusätzlich in meiner PostgreSQL-Datenbank.

\vspace{\vheader}
## Effiziente Groupbys
\vspace{\vheader}

Die Groupbys zur Datenaggregierung sind unterschiedlich schnell. Für die zeitliche und räumliche Übersicht von Verspätungen auf meiner Webseite ([bahnvorhersage.de/stats/stations](https://bahnvorhersage.de/stats/stations#content)) muss nach dem Groupby noch bekannt sein, um welchen Bahnhof es sich handelt. Die Bahnhöfe in die Aggregierung mit aufzunehmen, erweist sich als schwierig, da dies die Rechenzeit von zehn Minuten auf über 24 Stunden verlängert. Um das zu umgehen, verwende ich nicht die Bahnhofsnamen, sondern ein Label Encoding[^label_encoding] meines kategorischen[^categoricals] Datentyps Bahnhof.

[^label_encoding]: Jedem einzigartigen Wert wird eine Integerzahl zugeordnet.

[^categoricals]: Dask vergisst übrigens die Kategorien, sobald die Daten gespeichert werden. Den Workaround in der Dask Dokumentation habe ich hinzugefügt.

\vspace{\vheader}
# Machine learning
\vspace{\vheader}

Es gibt viele verschiedene Möglichkeiten, Verspätungen vorherzusagen. Spanninger et al. 2021 haben in einer Metastudie 71 verschiedene Veröffentlichungen zur Prognose von Zugverspätungen analysiert. Sie vergleichen die Prognosemodelle zunächst danach, ob sie datengetrieben oder ereignisgetrieben sind. Ereignisgetriebene Modelle Modellieren die Dynamik des Eisenbahnbetriebs. Für meine Problemstellung und meinen Datensatz arbeite ich datengetrieben.

Die datengetriebene Modelle basieren nicht auf der Modellierung der Dynamik des Eisenbahnbetriebs. Daher erzeugen sie typischerweise Ein-Schritt-Vorhersagen. Sie können auch den zehntnächsten Halt vorhersagen, ohne eine Vorhersage für die Haltestellen davor gemacht zu haben. Nach Spanninger et al. 2021 sind die meisten datengetriebenen Modelle maschinelle Lernmodelle. Außerdem ist ihre Vorhersagegenauigkeit wahrscheinlich höher als die von ereingnisgetriebenen Modellen.

Spanninger et al. 2021 unterteilen die Publikationen weiter nach ihrem Prognosehorizont von ultrakurzfristig (ultra-short-term) bis langfristig (long-term). Als langfristig wird dabei ein Prognosehorizont von mindestens vier Stunden definiert. Für mein Problem ist das gleichzeitig zu wenig und zu viel. Zugverbindungen werden oft Wochen im Voraus gebucht, also müssen die Vorhersagen auch Wochen im Voraus gemacht werden können. Es ist aber auch möglich, eine Verbindung erst wenige Minuten vor Abfahrt des ersten Zuges zu buchen. Die Optimierung eines Prognosesystems für diese beiden Extremfälle ist sehr aufwändig. Außerdem gibt es zu der langfristigen Prognose noch keine Forschung. Wu et al. 2021 untersuchen die Langfristprognosen mittels hybridem Long short-term memory (LSTM). Dieses Verfahren benötigt Echtzeitdaten. Auch Tatsui et al. 2021 verwendet LSTM. Oneto et al. 2018 untersuchte Zugverspätungsvorhersagesysteme aus einer Big Data Perspektive, mit dem Ziel, die Zugverspätung eines Zuges an seinem nächsten Kontrollpunkt vorherzusagen. Auch das benötigt Echtzeitinformationen. Diese Daten fehlen mir. Ich speichere nicht alle Verspätungsänderungen, die die Bahn meldet, sondern immer nur die aktuellste. So kann ich ein Machine-Learning-Modell nur auf die endgültigen Verspätungen trainieren und kann damit nicht die Verspätungsentwicklung vorhersagen.

Ich konzentriere mich daher auf sehr langfristige Vorhersagen. Damit meine ich Vorhersagen, die viele Tage in die Zukunft reichen können. Mit dem Kurzfristprognosesystem der Deutschen Bahn mitzuhalten, ist ohnehin fast unmöglich. Ich habe bei weitem nicht die Informationen über belegte Streckenabschnitte, aktuelle Störungen usw., wie sie der Bahn intern zur Verfügung stehen. Das Prognosesystem der Bahn wirkt subjektiv oft relativ ungenau, ist aber teilweise so gewollt. Denn bei der Prognose von Zugverspätungen gibt es noch ein weiteres Problem: Für die Fahrgastinformation ist es weitaus problematischer, wenn zu viel Verspätung vorhergesagt wird, als wenn zu wenig Verspätung vorhergesagt wird. Wenn zu viel Verspätung vorhergesagt wird, kann es passieren, dass Fahrgäste zu spät am Bahnhof eintreffen und ihren Zug verpassen, obwohl sie ihn ohne Probleme erreicht hätten.

\vspace{\vheader}
## Methodik
\vspace{\vheader}

Eine wichtige Entscheidung ist, was ich vorhersagen will. Ziel ist es, wie in @sec:intro beschrieben, die Anschlusssicherheit von Zugverbindungen zu bewerten. Ausschlaggebend für die Bewertung soll die Erreichbarkeit von Umstiege sein. Theoretisch wäre es also möglich, die Umstiege direkt zu bewerten. Ich habe mich dagegen entschieden, da ich dafür sinnvollerweise einen Datensatz mit realen Zugverbindungen benötige. Dieser müsste alle tatsächlich bei der Bahn gekauften Zugverbindungen enthalten. Da ich aber keine Ticketdaten von der Bahn bekomme, kann ich diesen Datensatz nicht generieren.

Deshalb prognostiziere ich die Verspätungen der einzelnen Züge, um daraus in einem zweiten Schritt die Anschlusssicherheit zu berechnen. Es hat sich bewährt, keine diskreten Werte per Regression vorherzusagen (ein Zug wird genau 5 Minuten Verspätung haben), sondern Klassenwahrscheinlichkeiten mit mehreren Klassifikationsmodellen. Die Klassen für die Vorhersagen werden getrennt für Ankunfts- ($\textrm{ar}$) und Abfahrtsverspätungen ($\textrm{dp}$) definiert:

$$
\textrm{Ankunftsverspätung} \leq n \in \{0, 1, ..., 14\} \land \textrm{Zug fällt nicht aus}
$$
$$
\textrm{Abfahrtsverspätung} \geq n \in \{0, 1, ..., 14\} \land \textrm{Zug fällt nicht aus}
$$

Im Folgenden werde ich zum Beispiel $\textrm{ar}_0$ verwenden, um die Vorhersagen des Modells zu bezeichnen, welches die Klasse $\textrm{Ankunftsverspätung} \leq 0 \land \textrm{Zug fällt nicht aus}$ vorhersagt.

Die Klassen sind mit $\leq$ und $\geq$ unterschiedlich definiert, da sie so leichter zu einem Verbindungsscore verrechnet werden können. Mehr dazu in [@sec:con_score].

Eine Prognose der Machine Learning Modelle liefert 30 Wahrscheinlichkeitswerte. 15 davon beschreiben die Wahrscheinlichkeiten, dass der Zug bei der Ankunft $\leq 0, 1, 2, ..., 14$ Minuten Verspätung hat (und der Halt nicht ausfällt), und 15 beschreiben die Wahrscheinlichkeiten, dass der Zug bei der Abfahrt $\geq 0, 1, 2, ..., 14$ Minuten Verspätung hat (und der Halt nicht ausfällt).

\vspace{\vheader}
## Baselines festlegen {#sec:baseline}
\vspace{\vheader}

Bevor ich Verspätungen vorhersage, möchte ich eine Grundlage dafür entwickeln, wann Vorhersagen gut sind und wann nicht.

Was ich als Baseline untersuche, ist eine allgemeine Statistik der Zugverspätungen. Mich interessiert nicht, wie viel mehr oder weniger Verspätung ein Zug in z.B. 30 Minuten hat; ich erfasse, wie viele Züge genau pünktlich sind, wie viele ausfallen oder mehr als zehn Minuten Verspätung haben. Als Metrik verwende ich die Majority Baseline. Diese beschreibt, wie viele Vorhersagen richtig sind, wenn man als statische Vorhersage die am häufigsten auftretende Klasse verwendet.

Die meisten Züge sind pünktlich. Etwa 55% der Züge haben genau 0 Minuten Verspätung, wie in @fig:per_delay durch die blauen Linien dargestellt. 93% sind pünktlich nach der Definition der Deutschen Bahn, d.h. sie haben maximal 5 Minuten Verspätung. Die Majority Baseline für Pünktlichkeit nach der Definition der Deutschen Bahn liegt also bei 93%. Wenn ein Modell weniger als 93% der Pünktlichkeit richtig vorhersagt, ist es unbrauchbar. Da es richtiger wäre anzunehmen, dass jeder Zug pünktlich ist. Verschiedene Teile der Bahn sind unterschiedlich pünktlich. Im Fernverkehr wird nur eine Pünktlichkeit von 76% erreicht. Zudem zeigen die grünen Linien in @fig:per_delay, dass Züge mit bereits hoher Verspätung öfters ausfallen als jene mit wenig Verspätung.

<div id="fig:verspätungsverteilung">

![Lineare Skala](plots/per_delay.png){#fig:per_delay width=45%}
![Logarithmischer Skala](plots/per_delay_loggy.png){#fig:per_delay_loggy width=45%}

Verspätungsverteilung
</div>

All das macht die Vorhersage der Verspätungen sehr schwer: Da 93% der Züge pünktlich sind, können fast alle unpünktlichen Züge als Extremwerte bezeichnet werden. Der Median der Verspätungen ist Null Minuten. Die Verspätungen sind stark rechtsschief. Diese Schiefe und der Median bei null Minuten machen Vorhersagen durch maschinelles Lernen sehr schwierig. Viele Lernverfahren haben Probleme mit der ungleichen Verteilung der Klassen.

\vspace{\vheader}
### Rebalancierung der Daten
\vspace{\vheader}

Als Lösung der unausgeglichenen Daten bestünde die Möglichkeit, die Daten durch Resampling auszugleichen[^rebalancing]. Dabei können entweder durch Undersampling so viele Daten gelöscht werden, bis gleich viele Züge verspätet und nicht verspätet sind, oder durch Oversampling so viele synthetische Daten hinzugefügt werden, bis gleich viele Züge verspätet und nicht verspätet sind. Beide Methoden sind leider nur dann sinnvoll, wenn die Unausgewogenheit der Daten nicht der Realität entspricht. Ich habe zunächst mit undersampled balancierten Daten gearbeitet, aber die Modelle, die auf balancierten Daten trainiert wurden, schneiden dann auf realen, nicht rebalancierten Daten in der Regel schlechter ab als die Majority Baseline. Sie entsprechen einfach nicht der "Realität".

[^rebalancing]: Baptiste Rocca: ["Handling imbalanced datasets in machine learning"](https://towardsdatascience.com/handling-imbalanced-datasets-in-machine-learning-7a0e84220f28)

\vspace{\vheader}
## Merkmalswichtigkeit {#sec:feature_importance}
\vspace{\vheader}

Welche Merkmale zur Vorhersage verwendet werden, hängt von der Verfügbarkeit und dem Informationsgehalt der Daten ab. Werden wenig informative Merkmale verwendet, kann das die Gefahr von over-fitting vergrößern. Die Merkmalswichtigkeit nach Info-Gain der verwendeten Merkmale ist in [@fig:feature_importance] abgebildet. Je heller eine Kachel ist, desto höher ist der Info-Gain durch das Merkmal für ein bestimmtes Modell.

<div id="fig:feature_importance">

![Wichtigkeit der Merkmale (Ankunft)](plots/feature_importance_ar.png){#fig:feature_importance_ar width=45%}
![Wichtigkeit der Merkmale (Abfahrt)](plots/feature_importance_dp.png){#fig:feature_importance_dp width=45%}

Info-Gain der Merkmale für die Verspätungsvorhersage
</div>

Das insgesamt wichtigste Merkmal ist die bereits gefahrene Streckenlänge. Diese ist bei allen Modellen hellorange. Des weiteren sind besonders die Standzeit am Bahnhof, der Betreiber der Züge, der Breitengrad des Halts und der Zugtyp für die Vorhersage von Bedeutung. In [@sec:beispieltag] wird gezeigt, dass auch eher unwichtigere Merkmale einen sehr positiven Effekt auf die Vorhersagequalität haben können. Die Genauigkeit der Vorhersagen auf Testdaten sinkt, wenn eines der gerade verwendeten Merkmale entfernt wird. Wetterdaten hingegen haben keine deutliche Auswirkung auf die Genauigkeit der Modelle und werden daher nicht mehr verwendet.

\vspace{\vheader}
## Machine Learning Modell
\vspace{\vheader}

Ein weiterer wichtiger Schritt ist die Auswahl des geeigneten maschinellen Lernmodells. Für die von mir gewählte Methodik ist prinzipiell zunächst jedes Modell geeignet, das eine Klassifikation unterstützt und Klassenwahrscheinlichkeiten ausgeben kann. Also z.B.: "Wie wahrscheinlich ist es, dass ein Zug mehr als fünf Minuten Verspätung hat?". Spanninger et al. 2021 haben in @fig:classification_of_train_delay_prediction_approaches dargestellt, welche Modelle in der bisherigen Forschung verwendet wurden.

![Spanninger et al.: Classification of train delay prediction approaches (Nur Data-Driven)](plots/Spanninger%20et%20al.:%20Classification%20of%20train%20delay%20prediction%20approaches.png){#fig:classification_of_train_delay_prediction_approaches width=50%}

Wie in der Arbeit 2020 in Tabelle 4.1 dargestellt, hatte ich herausgefunden, dass ein Random Forest bei meinen Daten besser abschneidet als ein Decision Tree und extraTrees. Damals hatte ich auch die Support Vector Machine getestet. Dieses Modell hatte aber so schlecht abgeschnitten, dass ich es nicht einmal in die schriftliche Arbeit aufgenommen habe. Neuronale Netze fand ich immer sehr vielversprechend. Ich habe mich viel mit der Architektur und den Trainingsverfahren für verschiedene neuronale Netze beschäftigt. Die Vorhersagen der neuronalen Netze waren aber meistens falsch. Das liegt wahrscheinlich daran, dass die neuronalen Netze mit den unbalancierten Daten nicht gut trainiert werden können. Hier ist z.B. der Random Forest deutlich besser geeignet. 2021 habe ich den Random Forest gegen ein XGBoost-Modell ausgetauscht, da dieses noch einmal deutlich besser abschneidet. Die Genauigkeit des Modells $\textrm{ar}_0$ mit XGBoost ist ca. 10 % besser ab als ein Random Forest.

Für diese habe ich ein Hyperparametertuning mit dem Framework Optuna[^optuna] durchgeführt. (Zeitaufwand ca. 40 Stunden, Rechenzeit ca. 1,5 Wochen)

[^optuna]: Hyperparameter optimization framework: [https://optuna.org/](https://optuna.org/)

\vspace{\vheader}
## Autotraining der Machine Learning Modelle
\vspace{\vheader}

Die Modelle sollen immer auf den Daten der letzten sechs Wochen trainiert sein. Dadurch passt sich das Modell automatisch an die Jahreszeit und die aktuelle Verkehrssituation an. Auch langfristige Baumaßnahmen werden, sofern sie in den Daten abgebildet sind, von dem Modell erfasst. Für eine hohe Qualität der Vorhersagen müssen die Vorhersagemodelle so aktuell wie möglich sein.

Damit die Modelle trainiert werden können, müssen zum Trainingszeitpunkt alle akutellen Daten geparst vorliegen. Aus diesem Grund werden alle Daten in Echtzeit geparst. Die Notwendigkeit des komplexen Aufbaus des Parsers kommt also unter anderem davon, dass ich das Projekt auf meiner Webseite für alle verfügbar und aktuell halten möchte.

Das Training selbst ist im Prinzip relativ einfach. Ein bereits in meinem Projekt entwickeltes Modul lädt die Daten aus der Datenbank herunter und speichert sie im Parquet-Format. Die Daten werden dann später über dieses Modul wieder geladen, gefiltert und neu aufbereitet, sodass der Machine Learner darauf trainiert werden kann.

Hier ergab sich ein weiteres Problem: Alle meine Programme laufen auf meinem Server in Container verpackt in einem Kubernetes Cluster, welches damals standardmäßig keinen GPU unterstützt hat. Die GPU wird aber für das Training der Machine Learning Modelle dringend benötigt, da der Trainingsprozess sonst sehr langsam ist. Die Installation der GPU-Unterstützung war damals noch in der Beta, und bei mir wegen vielen unterschiedlichen Fehlern sehr zeitintensiv.

\vspace{\vheader}
## Verbindungsscore {#sec:con_score}
\vspace{\vheader}

Der Verbindungsscore ist das Endergebnis meiner Verspätungsprognose, die auf meiner Webseite angezeigt wird. Er beschreibt die Wahrscheinlichkeit, einen Anschluss zu bekommen.

Ich gehe dafür davon aus, dass zwei Minuten reelle Umsteigezeit benötigt werden, damit der Anschluss erreicht wird[^tra_time]. Die Summe der Wahrscheinlichkeiten aller Fälle, bei denen 2 Minuten Umsteigezeit bleiben, ergibt den Verbindungsscore. Hat ein ankommender Zug bei fünf Minuten Umsteigezeit beispielhaft fünf Minuten Verspätung, so muss der abfahrende Zug mindestens zwei Minuten verspätet abfahren, damit zwei Minuten Umsteigezeit bleiben. 

[^tra_time]: Diese Annahme ist oft falsch. Eine Datenbasis für bessere Annahmen fehlt.

\vspace{\vheader}
# Genauigkeit der Vorhersagen
\vspace{\vheader}

## Genauigkeit der Verspätungsvorhersagen {#sec:accuracy}
\vspace{\vheader}

Die Prognose von Zugverspätungen ist alles andere als einfach. Wie in @sec:baseline beschrieben, liegen die Majority Baselines für viele Vorhersagen weit über 90%. Um die Modelle zu bewerten, berücksichtige ich zusätzlich, dass die Modelle täglich neu trainiert werden. Ich möchte also nicht nur einmal meine 30 verschiedenen Modelle bewerten, sondern auch, wie sich ihre Genauigkeit über die Zeit verändert. Dabei bewerte ich die Modelle danach, wie gut sie in einem "echten" Szenario abschneiden. Dazu habe ich jeweils ein Datum als Train-Test-Split gewählt. Ich bezeichne ein Modell nach dem Datum des Train-Test-Splits. Die Modelle werden mit den Daten der sechs Wochen vor diesem Datum trainiert und mit den Daten des Tages nach diesem Datum getestet.

In @fig:accuracy sind die Majority Baselines und die Genauigkeit aller 30 verwendeten Modelle für den Zeitraum vom 1.5. bis 1.9.2022 dargestellt. Für jeden Tag wurden die Modelle mit dem jeweiligen Tag als Train-Test-Split trainiert und ausgewertet.

<div id="fig:accuracy">

![](plots/stats_ar_0_6.png){#fig:stats_ar_0_6 width=45%}
![](plots/stats_dp_0_6.png){#fig:stats_dp_0_6 width=45%}

![](plots/stats_ar_6_12.png){#fig:stats_ar_6_12 width=45%}
![](plots/stats_dp_6_12.png){#fig:stats_dp_6_12 width=45%}

![](plots/stats_ar_12_15.png){#fig:stats_ar_12_15 width=45%}
![](plots/stats_dp_12_15.png){#fig:stats_dp_12_15 width=45%}

Genauigkeit der Verspätungsvorhersagen
</div>

In @fig:stats_ar_0_6 ist an der bauen Linie (gestrichelt) zu sehen, dass die Majority Baseline für das $\textrm{ar}_0$ bei gut 50 % liegt. Das bedeutet, dass gut 50 % der Züge genau null Minuten Verspätung haben. Die blaue Linie zeigt, wie gut die Vorhersagen des $\textrm{ar}_0$ sind. Die Vorhersagen sind zu knapp 80 % richtig. Mein Modell kann mit fast 80 % Genauigkeit vorhersagen, ob ein Zug genau pünktlich ist. Das ist im Vergleich zur Majority Baseline sehr gut. Die Baseline (orange, gestrichelt) für das $\textrm{ar}_1$ ist deutlich höher, bei durchschnittlich knapp 70 %. Die Vorhersage des $\textrm{ar}_1$ (orange) ist zwar noch deutlich besser als die Baseline, allerdings bei weitem nicht mehr so beeindruckend wie das $\textrm{ar}_0$. Je höher die vorhergesagte Verspätung der Modelle, desto höher die Baseline, desto geringer die Verbesserung der Vorhersage im Vergleich zur Baseline. Bei den 12-, 13- und 14-Minuten-Modellen in @fig:stats_ar_12_15 und @fig:stats_dp_12_15 gibt es kaum noch einen Unterschied. Die konkreten Werte der Vorhersage sind nur marginal über der Baseline. Aus der Reihe fällt das $\textrm{dp}_0$ in @fig:stats_dp_0_6. Das liegt daran, dass dieses wie in [@sec:con_score] beschrieben als 
$$
\textrm{Abfahrtsverspätung} \geq 0 \land \textrm{Zug fällt nicht aus}
$$
definiert ist. Es sagt daher effektiv nur die Ausfälle vorher. Da die Modelle für hohe Verspätungsvorhersagen in aller Regel nicht wirklich besser als die Baseline performen, verwende ich nur Modelle für bis zu 14 Minuten Vorhersage. 2021 hatte ich jeweils 40 Modelle verwendet.

Im Vergleich zur Arbeit 2021 konnte ich die Genauigkeit der Vorhersagen deutlich steigern. Das $\textrm{ar}_0$-Modell hatte 2021 noch eine Genauigkeit von knapp 70 %. Durch die Verbesserung der Hyperparameter, des Trainingszeitraums und der Datenqualität erreiche ich nun eine Genauigkeit von 80 %. Die Modelle für höhere Verspätungen haben sich nur marginal verbessert, da diese sowieso nur knapp über der Baseline liegen.

\vspace{\vheader}
## Genauigkeit der Verspätungsvorhersagen an einem Beispieltag {#sec:beispieltag}
\vspace{\vheader}

Die Ergebnisse von [@sec:accuracy] zeigen, dass die Vorhersagen gut sind. Um das Vertrauen in die Modelle noch weiter zu stärken, wäre es sinnvoll zu zeigen, wie sich einzelne Merkmale auf die Vorhersagen auswirken. In [@fig:over_day] ist die Pünktlichkeit[^puenktlichkeit] aller Züge am 1.9.2022 dargestellt. Die rote und die orange Linie zeigen, dass die Pünktlichkeit besonders zu den Stoßzeiten sinkt. Zudem sind Züge morgens pünktlicher als abends.

[^puenktlichkeit]: Maximal fünf Minuten Verspätung

<div id="fig:predictions_over_day">

![Pünktlichkeit am 1.9.2022](plots/over_day_on_time_2022_09_01.png){#fig:over_day width=45%}
![Pünktlichkeitsvorhesage für den 1.9.2022](plots/over_day_predictions_2022_09_01.png){#fig:over_day_predictions width=45%}

Vorhersagen im Vergleich zum tatsächlichen Tagesverlauf
</div>

[@fig:over_day_predictions] zeigt die prognostizierte Pünktlichkeit für den 1.9.2022. Die vorhergesagte Pünktlichkeit ist der tatsächlichen Pünktlichkeit sehr ähnlich. Das Maschinelle Lernmodell hat offensichtlich den Zusammenhang zwischen der Tageszeit und der Pünktlichkeit erkannt. Und das, obwohl die Minute seit Tagesbegin laut [@sec:feature_importance] keinen besonders großen Einfluss auf die Pünktlichkeit hat. Das [@fig:over_day_predictions] und [@sec:feature_importance] nicht identisch sind, resultiert daraus, dass die Vorhersagen nicht zu 100 % genau sind.

\vspace{\vheader}
## Genauigkeit des Verbindungsscores
\vspace{\vheader}

Entscheidend für die Qualität der Vorhersagen auf meiner Webseite ist die Genauigkeit des Verbindungsscores. Ich unterteile die Analyse des Verbindungsscores in zwei Teile. Die Daten in den folgenden Abbildungen sind vom 1.9.2022. Die Vorhersagen stammen von Modellen, die auf den Daten der letzten sechs Wochen vor dem 1.9. trainiert wurden.

Für die Auswertung habe ich mögliche Umsteigeverbindungen aus den Daten extrahiert. Dazu habe ich die Daten nach Bahnhöfen sortiert und dann in den Bahnhöfen nach möglichen Verbindungen mit zwei bis zehn Minuten Umsteigezeit gesucht[^note_connecting_trains].

[^note_connecting_trains]: Was trivial klingt, ist bei den großen Datenmengen unglaublich kompliziert. Z.B. die Sortierung nach Bahnhöfen muss in einzelnen Abfragen nach Bahnhöfen erfolgen, sonst müssten alle Daten auf einmal in den Arbeitsspeicher geladen werden, was bei Big Data nie eine Option ist.

\vspace{\vheader}
## Fünf Minuten Umsteigezeit
\vspace{\vheader}

Ich möchte den Verbindungsscore unabhängig von der Umsteigezeit bewerten. Ich möchte die Frage beantworten, ob der Verbindungsscore in der Lage ist, Verbindungen mit gleicher Umsteigezeit nach ihrer Qualität zu sortieren. @fig:con_score_5_min_planned_transfer_time zeigt, dass alle Verbindungen planmäßig 5 Minuten Umsteigezeit haben.

<div id="fig:con_score_5_min">

![Fünf Min geplante Umsteigezeit](plots/con_score_5_min_planned_transfer_time.png){#fig:con_score_5_min_planned_transfer_time width=45%}
![Tatsächliche Umsteigezeit](plots/con_score_5_min_real_transfer_time.png){#fig:con_score_5_min_real_transfer_time width=45%}

Verbindungsscore bei fünf Minuten Umsteigezeit
</div>

Die rote Trendlinie in @fig:con_score_5_min_real_transfer_time hat eine positive Steigung, also gibt die Vorhersage meiner KI auch unabhängig von der Umsteigezeit einen Informationsgewinn, den die Nutzer:innen meiner Webseite verwenden können. Außerdem beträgt der Rangkorrelationskoeffizient nach Spearman $0.102$. Der P-Wert des Korrelationstests beträgt $3 \cdot 10^{-15}$. Daraus folgt, dass die Korrelation mit hoher Sicherheitswahrscheinlichkeit signifikant ist. Die beobachtete Korrelation ist also nicht zufällig. Bei einem Verbindungsscore von 90 bis 100 % werden 95 % der Anschlüsse erreicht. Ist er hingegen zwischen 60 und 70 %, klappen nur 85 % der Verbindungen. Der Verbindungsscore ist also keine Wahrscheinlichkeit, dass eine Verbindung funktioniert, ist bei besseren Verbindungen aber tendentiell höher und bei schlecheren Verbindungen tendentiell niedriger.

Wie immer gibt es extreme Ausreißer. Dies sind Fälle, in denen die Verspätungen überhaupt nicht vorhersagbar sind.

\vspace{\vheader}
## Zwei bis zehn Minuten Umsteigezeit
\vspace{\vheader}

Auf meiner Webseite kann man leicht sehen, welche Verbindungen wie sicher sind. Die Umsteigezeit spielt dabei eine große Rolle. Je länger die Umsteigezeit, desto wahrscheinlicher ist es, dass der Anschluss erreicht wird. Wie viel eine oder zwei Minuten mehr Umsteigezeit ausmachen, ist allerdings schwer abzuschätzen. Meine Prognosen geben auch hier einen Überblick. Ich untersuche daher auch, wie sich der Verbindungsscore bei einer Umsteigezeit von zwei bis zehn Minuten verhält.

<div id="fig:con_score_2_to_10_min">

![Zwei bis zehn Min geplante Umsteigezeit](plots/con_score_2_to_10_min_planned_transfer_time.png){#fig:con_score_2_to_10_min_planned_transfer_time width=45%}
![Tatsächliche Umsteigezeit](plots/con_score_2_to_10_min_real_transfer_time.png){#fig:con_score_2_to_10_min_real_transfer_time width=45%}

Verbindungsscore bei zwei bis zehn Minuten Umsteigezeit
</div>

In @fig:con_score_2_to_10_min_planned_transfer_time ist zu erkennen, dass grundsätzlich Verbindungen mit mehr Umsteigezeit besser bewertet werden. Vergleicht man diese mit den tatsächlichen Umsteigezeiten in @fig:con_score_2_to_10_min_real_transfer_time, so erkennt man, dass die rote Trendlinie bei Berücksichtigung der tatsächlichen Umsteigezeit noch steiler verläuft. Auch in diesem realistischeren Beispiel bringt meine Prognose also einen zusätzlichen Informationsgewinn.

<!-- 5 min connections con_score stats:
con_score between 0.0 and 0.1: 48.48% (33 data points)
con_score between 0.1 and 0.2: 53.61% (166 data points)
con_score between 0.2 and 0.3: 66.28% (341 data points)
con_score between 0.3 and 0.4: 79.52% (664 data points)
con_score between 0.4 and 0.5: 78.90% (967 data points)
con_score between 0.5 and 0.6: 81.57% (2160 data points)
con_score between 0.6 and 0.7: 86.74% (4503 data points)
con_score between 0.7 and 0.8: 91.30% (8836 data points)
con_score between 0.8 and 0.9: 93.71% (19847 data points)
con_score between 0.9 and 1.0: 95.63% (21083 data points)
2 - 8 min connections con_score stats:
con_score between 0.0 and 0.1: 31.03% (58 data points)
con_score between 0.1 and 0.2: 41.56% (231 data points)
con_score between 0.2 and 0.3: 46.32% (516 data points)
con_score between 0.3 and 0.4: 62.54% (961 data points)
con_score between 0.4 and 0.5: 69.63% (1643 data points)
con_score between 0.5 and 0.6: 76.30% (2953 data points)
con_score between 0.6 and 0.7: 83.45% (5062 data points)
con_score between 0.7 and 0.8: 89.70% (7699 data points)
con_score between 0.8 and 0.9: 93.78% (14043 data points)
con_score between 0.9 and 1.0: 96.16% (25433 data points) -->

\vspace{\vheader}
# Webseite und Infrastruktur
\vspace{\vheader}

## Infrastruktur
\vspace{\vheader}

Meine Webseite läuft produktiv. Die Hardware besitze und pflege ich aus Kostengründen selber. Meine Probleme benötigen viel CPU, RAM, SSD und GPU. Daher betreibe ich einen eigenen Server mit 32-Kern-Prozessor, 160 GB Arbeitsspeicher, 2 TB SSD (RAID 0) und einer NVIDIA 3080 Grafikkarte.

\vspace{\vheader}
## Tech Stack
\vspace{\vheader}

Die Postgres Datenbank läuft aus Performancegründen bare metal auf dem Server, alles andere ist in einem Kubernetes Cluster deployed. In [@fig:tech_stack] sind alle auf dem Server laufenden Prozesse abgebildet.

![Tech Stack](img/tech_stack.png){#fig:tech_stack  width=75%}

\vspace{\vheader}
## Webseite
\vspace{\vheader}

Die Entwicklung der Webseite mit VueJS und Python-Flask macht ein Drittel des gesamten Projektaufwands aus und bekommt täglich ca. 100 Aufrufe.

\vspace{\vheader}
# Open-Source-Beiträge {#sec:opensource}
\vspace{\vheader}

Da mein Projekt relativ spezielle Anforderungen hat, musste ich mir fehlende Funktionen in einigen Open-Source-Frameworks selber hinzufügen. Hier ist eine Liste meiner Beiträge:

\vspace{\vheader}
## Dask
\vspace{\vheader}

- [Remove sqlachemy 1.3 compatibility](https://github.com/dask/dask/pull/9695)
- [Moving to SQLAlchemy >= 1.4](https://github.com/dask/dask/pull/8158)
- [fix index_col duplication if index_col is type str](https://github.com/dask/dask/pull/7661)
- [dd.from_sql_table() duplicates index with SQLAlchemy==1.4.1](https://github.com/dask/dask/issues/7436)
- [Example for working with categoricals and parquet](https://github.com/dask/dask/pull/7085)
- [Added bytes/row calculation when using meta](https://github.com/dask/dask/pull/6585)
- [dd.read_sql_table could calculate npartitions even if meta is defined](https://github.com/dask/dask/issues/6569)

\vspace{\vheader}
## OSMnx
\vspace{\vheader}

- [Added note to warn against #791](https://github.com/gboeing/osmnx/pull/803)
- [Loading large areas won't necessarily load the whole area](https://github.com/gboeing/osmnx/issues/791)

\vspace{\vheader}
## Matplotlib
\vspace{\vheader}

- [Added example on how to make packed bubble charts](https://github.com/matplotlib/matplotlib/pull/18223)

\vspace{\vheader}
# Zusammenfassung
\vspace{\vheader}
In 3,5 Jahren Arbeit habe ich den wahrscheinlich größten deutschen Verspätungsdatensatz außerhalb der Bahn gesammelt. In vielen Iterationen entwickelte ich darauf ein Machine Learning System, überprüfte und verbesserte es immer wieder, um damit zukünftige Verspätungen vorherzusagen. Diese werden genutzt, um möglichst anschlusssichere Zugverbindungen zu finden. All das ist über meine Webseite [bahnvorhersage.de](https://bahnvorhersage.de/) einfach, rund um die Uhr und auf jedem Gerät verwendbar.

\vspace{\vheader}
# Ausblick
\vspace{\vheader}
In Zukunft möchte die Vorhersagen nutzen, um ein eigenes Routingsystem basierend auf gtfs-Fahrplandaten für ganz Deutschland zu entwickeln. Nutzer:innen sollen zu schlechten Verbindungen gleich bei der Buchung alternative Verbindungen vorgeschlagen bekommen. In die Bewertungen wird mit einfließen, wie gut oder schlecht die Alternativverbindungen sind. Fährt 15 Minuten nach einem verpassten Anschluss ein weiterer Zug, ist dies nicht gravierend. Außerdem ist es in einigen Einzelfällen möglich, Verbindungen zu finden, die laut Fahrplan nicht funktionieren sollten, da ohne Verspätungen nicht ausreichend Umsteigezeit vorhanden wäre. Dadurch lassen sich Zugverbindungen finden, die schneller sind, als eigentlich möglich wäre.

\newpage{}

\vspace{\vheader}
# Literatur
\vspace{\vheader}

Döllmann, Meurers, TrAIn_Connectione_Prediction (Jugend Forscht 2021). Available at: [https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202021.pdf](https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202021.pdf)

Döllmann, Meurers, TrAIn_Connectione_Prediction (Jugend Forscht 2020). Available at: [https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202020.pdf](https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202020.pdf)

Spanninger, Thomas and Trivella, Alessio and Büchel, Beda and Corman, Francesco, A Review of Train Delay Prediction Approaches (November 16, 2021). Available at SSRN: [http://dx.doi.org/10.2139/ssrn.3964737](http://dx.doi.org/10.2139/ssrn.3964737)

Wu, J.; Du, B.; Wu, Q.; Shen, J.; Zhou, L.; Cai, C.; Zhai, Y.; Wei, W.; Zhou, Q. A Hybrid LSTM-CPS Approach for Long-Term Prediction of Train Delays in Multivariate Time Series. Future Transp. 2021, 1, 765–776. [https://doi.org/10.3390/futuretransp1030042](https://doi.org/10.3390/futuretransp1030042)

TATSUI, Daisuke & NAKABASAMI, Kosuke & KUNIMATSU, Taketoshi & SAKAGUCHI, Takashi & TANAKA, Shunichi. (2021). Prediction Method of Train Delay Using Deep Learning Technique. Quarterly Report of RTRI. 62. 263-268. 10.2219/rtriqr.62.4_263. 

Luca Oneto, Emanuele Fumeo, Giorgio Clerico, Renzo Canepa, Federico Papa, Carlo Dambra, Nadia Mazzino, Davide Anguita, Train Delay Prediction Systems: A Big Data Analytics Perspective, Big Data Research, Volume 11, 2018, Pages 54-64, ISSN 2214-5796, [https://doi.org/10.1016/j.bdr.2017.05.002](https://doi.org/10.1016/j.bdr.2017.05.002).

\vspace{\vheader}
# Unterstützungsleistungen
\vspace{\vheader}

Ich möchte mich in erster Linie bei Marius De Kuthy Meurers bedanken, mit dem ich lange Zeit zusammen an diesem Projekt gearbeitet habe.

Vielen Dank an alle, die mich bei meiner Arbeit unterstützt haben.

- **Langfassung**: Vielen Dank an alle, die die Langfassung Korrektur gelesen haben und mir Tipps gegeben haben.
- **Uni Tübingen / MPI für Intelligente Systeme / BWKI**: Bereitstellung von Rechenleistung mittels CPUs und GPUs.
- **DB Analytics**: Einblicke in den Bahnbetrieb sowie Antworten auf bahninterne Fragen. Außerdem gewinnt meine Kontaktperson Jochen Völker den Low Latency Award (latency ≤ 10min) beim Beantworten von E-Mails.
- **OpenStreetMaps**: Ohne sehr viele Menschen, die eine öffentliche und extrem genaue Karte des Deutschen Schienennetzes erstellt haben und immer erneuern und verbessern, hätte ich vermutlich immer noch kein routingfähiges Streckennetz.
- **SFZ Eningen**: Mein Server steht zurzeit im SFZ Eningen. Vielen Dank an das SFZ, das bisher auch alle Stromkosten übernommen hat.
- Vielen Dank an **Herrn Glück**, der mich bei der Anschaffung eines eigenen "Supercomputers" sehr unterstützt hat. Finanzielle Unterstützung dafür erhielt ich von:
    - Bosch
    - Herrn Mörike
    - Kepi-Föderkreis
    - Micron