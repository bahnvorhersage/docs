### Done:
- Vernetzung mit ÖPNV-Menschen

### Doing:
- GTFS nochmal ganz genau anschauen
- Langzeitdatenspeicherung in ein GTFS-ähnliches Format übertragen

### Motivation & Challenges:
- Speicherplatz eher knapp
- Start der Föderphase

---

### Done:
- Typisierte IRIS Schnittstelle

### Doing:
- IRIS to GTFS parser
- GTFS archive format

### Motivation & Challenges:
- GTFS ziemlich ungeeignet, um daten zu archivieren

---

### Done:
- Bewerbung bei zwei Wettbewerben zur "Werbung" für das Projekt
- Zahlungsanforderung

### Doing:
- IRIS to GTFS parser (done, but still to slow)
- IRIS to GTFS realtime
- ML training from GTFS
- Realtime GTFS parser

### Motivation & Challenges:
- Postgresql upsert sehr langsam
- Große Datenmengen nerven mich

---

### Done:
- Router prototype that finds fastest route
- BMBF logo on website
- simple graph visulisation of routes for debugging
- set a reminder to do the next update on time

### Doing:
- Truncate bad routes during routing
- compute subset of usefull routes
- bring new crawler, parsers and stuff to production

### Motivation & Challenges:
- Postgres is still being slow
- Routing is slow
- Routing is hard if the goal is not the best connection, but all good connections


---

### Done:
- All newly written code in production
    - New timetable crawler with about 1 % of cpu time compared to the old one
    - New timetable deviation crawler with same improvement
    - New monitor bot to monitor crawlers
    - Due to new cartopy version, all docker-images could be merged and massivly simplified. Webserver, crawlers, monitor, ... can now all use the same image  
- sorting arrival and departure time prediction to get a more realistic dependencie between transfer time and connection score
- Submitted a talk about this projekt to the 37c3
- Took a week of

### Doing:
- Truncate bad routes during routing
- compute subset of usefull routes
- Talk to mathematitions to solve problems

### Motivation & Challenges:
- Routing is hard if the goal is not the best connection, but all good connections


---

### Done:
- Networking with a projekt trying to build a europe wide transit planner from vienna
- Networking with NVBW (Nahverkehrsgesellschaft Baden-Württemberg) who might be interested in integrating my project into their journey planner
- Zwischengespräch
- Networking with AI professors in Tübingen
- First coaching session
- Some kind of a new concept for journey routing

### Doing:
- Implementing journey routing

### Motivation & Challenges:
- Routing is hard if the goal is not the best connection, but all good connections
- There are many people I talk to that describe the exact problem I am trying to solve
- Organisations like the NVBW are interested in the Projekt, which might lead to long term funding or something similar

---

### Done:
- FIX: Routing now properly includes regional service only routes for 9€ ticketing
- coaching
- Two very basic design mockups
- very little user testing an feedback to the designs 

### Doing:
- Implementing journey routing
- Working on a frontend design

### Motivation & Challenges:
- Many things are starting to work

---

### Done:
- Build a routing engine to find good connections
- A frontend design idea that looks very promising

### Doing:
- Building a routing engine to find connections at speed
- Defining what a alternative connection is an implementing it

### Motivation & Challenges:
- Comuting during DB strike showed me once again why I am trying to improve passanger infomation and how it could help myself


---


### Done:
- Massiv 10x speedup in routing
- Alternative routing
- Frontend display of connections
- Migrated frontend from vue-cli to vite and from vuex to pinia

### Doing:
- Refining frontend ui
- building an api to use routing
- integrate api in frontend to display connections the user searched for
- enrich routes with all infomation, a user might need (platforms, ...)


### Motivation & Challenges:
- After a long time, i can see visuals of what I did the whole time
- Integration hell is starting, and the demo day is getting closer and closer


---


### Done:
- API for routing working
- Frontend is now interactive but displays ab big warning, that many things don't work yet
- Networking with Robin from traines.eu
- Migrating old data to a new database table
- Added about 150 train stations that were missing

### Doing:
- Refining frontend ui
- enrich routes with all infomation, a user might need (platforms, ...)
- parsing timetables in realtime


### Motivation & Challenges:
- I currently don't have any time as I have to write exams


---


### Done:
- Added platforms to journeys
- Migrated older data to a new format using a different hashing algorithm
- Parse timetables as soon as they come in
- Calculate transfers between stations
- Use transfers in routing
- Show journey details by clicking on journey
- Show some routing errors in frontend

### Doing:
- Make some slides
- Deploy everything
- Relax Pareto domination to filter out some (not so usefull) journeys
