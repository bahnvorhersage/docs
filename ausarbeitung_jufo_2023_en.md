---
title: Bahn-Vorhersage
author: Theo Döllmann
date: "11. März 2023"
geometry: top=2.5cm, bottom=2cm, left=2.5cm, right=2.5cm
numbersections: true
toc: true
# toc-title: Inhaltsverzeichnis
colorlinks: true
listings: True

# figureTitle: Abbildung
# figPrefix: Abb.

# tableTitle: Tabelle
# tblPrefix: Tab.

# secPrefix: Abschn.

# eisvogel template
titlepage: true
titlepage-rule-height: 8
titlepage-logo: img/IC.png
toc-own-page: true
---

<!-- 
compile to pdf:
pandoc -H head.tex -V lang=en --pdf-engine=xelatex --template eisvogel --filter pandoc-crossref --listings -s -o Langfassung_Bahnvorhersage_2023_en.pdf README_en.md

compress pdf to 300 dpi:
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/prepress -dNOPAUSE -dQUIET -dBATCH -sOutputFile=Langfassung_Bahnvorhersage_2023_en_300_DPI.pdf Langfassung_Bahnvorhersage_2023_en.pdf
-->

\pagenumbering{roman}

\newcommand\vheader{-2ex}


\vspace{\vheader}
# Abstract {.unnumbered .unlisted}
\vspace{\vheader}


In three and a half years of work, I was able to show with my Big Data project Bahn-Vorhersage that it is entirely possible to predict the connection reliability of train services several days in advance. Through an elaborate data fusion of historical train movements, station information and data from the European rail network, I created a data set with over 470 million data points. With this dataset, which can only be processed with Big Data tools, I taught an XGBoost machine learning model to predict train delays. With these predictions, I can predict the connection reliability of future train connections on my website [bahnvorhersage.de](https://bahnvorhersage.de/). I was able to prove that there is a positive correlation between the connection forecasts I make and the actual transfer times.

Because of long delays, public transport in Germany is unattractive for many people. I will not be able to increase the reliability of the railways with my project (this would require above all political will and massive investment in the rail infrastructure), but users of my website can specifically avoid train connections that are particularly prone to disruptions.

\vspace{\vheader}
# Open Source {.unnumbered .unlisted}
\vspace{\vheader}

The project is open source. The entire source code is linked on my website: [bahnvorhersage.de/opensource](https://bahnvorhersage.de/opensource#content)

\vspace{\vheader}
# Website {.unnumbered .unlisted}
\vspace{\vheader}

The website for the project: [bahnvorhersage.de](https://bahnvorhersage.de/)

\newpage{}

\pagenumbering{arabic}

\vspace{\vheader}
# Introduction {#sec:intro}
\vspace{\vheader}

Almost four years ago, originally for the _Federal Artificial Intelligence Competition_ 2019[^bwki], my then project partner Marius and I decided to predict train delays using Artificial Intelligence.

[^bwki]: In 2019, Marius and I won the competition with this project: [https://www.bw-ki.de/rueckblick#c94](https://www.bw-ki.de/rueckblick#c94)

On my website [bahnvorhersage.de](https://bahnvorhersage.de/) you can search and find train connections, similar to the site of die Bahn (German Railways). In addition to the well-known connection details of the railway, my website gives a "connection score" for each connection. This describes the probability of reaching all connecting trains.

For two and a half years, I have been collecting a huge, terabyte-sized dataset of delay data from all over Germany and beyond around the clock. I combine this with information about stations and the European rail network. A machine learning process uses this data to predict train delays for the next few weeks from past events.

The particular difficulty of this project lies in the database and the size of the data set. In addition, train delays develop chaotically, which makes prediction extremely difficult. The reasons for delays are manifold: lack of personnel, trees on the tracks or defective vehicles. All these reasons are unpredictable. As a result, the delays themselves also develop almost unpredictably.

The actual causes of train delays lie in the car-fixated decisions of the last 20 years, which were decisively shaped by the CDU/CSU/FDP. The railways lack one thing above all: track capacity. Many railway lines are over 100 percent full, which leads to traffic jams and delays. The trains then no longer have a chance to make up for a delay.

\vspace{\vheader}
# Data sources
\vspace{\vheader}


In order to develop a system for predicting train connections, I need historical real-time data of train arrival and departure times over an extended period of time. I also need as rich data as possible on the various factors that lead to delays so that the machine learning procedure can take them into account when predicting delays. Data turned out to be the main problem of the project. Past delay data is not available in Germany. Information on the rail infrastructure, which has a significant influence on delays, is only ever published for parts of the infrastructure with large data gaps and low quality[^data_sources_strecke].


[^data_sources_strecke]: Data sources are listed in [@sec:route_data].


\vspace{\vheader}
## Historical train runs
\vspace{\vheader}


Finding a suitable data source for past train delays proved to be extremely difficult. The only solution for those external to the railway seems to be to collect the data live. There are several possible railway interfaces for this:


- HAFAS[^hafas]
- IRIS[^iris]
- EFA[^efa]
  
However, the HAFAS endpoint has a rate limit that is too low. Since I could not find any documentation for the EFA, I decided to use the IRIS. With this, each station board from each station has to be polled individually at least every 2 minutes. In the end, I do it quite similar to what David Kriesel shows in a lecture "BahnMining - Pünktlichkeit ist eine Zier" [^bahn_mining], except that in my work Jugend Forscht (2021, chapter 3) I had found that Mr. Kriesel's data is error-prone [^davids_error]. Therefore, I do not query the live data of the stations every hour like he does, but every two minutes, because the data is deleted after a train has left. Furthermore, I query 9238 stations, which is about 30 times more than Mr. Kriesel. That makes:


[^hafas]: The HaCon **Fa**hrplan-**A**uskunfts-**S**ystem (HAFAS) is a software for timetable information from the company HaCon (Hannover Consulting). HAFAS is behind [https://bahn.de/](https://bahn.de/) and the DB-Navigator, for example. 


[^iris]: The **I**nterne-**R**eisenden-**I**nformations**S**system (IRIS for short) provides station boards with data, such as this Tübingen departure board: [https://iris.noncd.db.de/wbt/js/index.html?typ=ab&bhf=TT](https://iris.noncd.db.de/wbt/js/index.html?typ=ab&bhf=TT)


[^efa]: The **E**lektronische **F**ahrplan**a**uskunft (EFA) is a software product of the company MENTZ GmbH. E.g., used here: [http://www.efa-bw.de/](http://www.efa-bw.de/)


[^bahn_mining]: Lecture by David Kriesel on railway punctuality: [https://media.ccc.de/v/36c3-10652-bahnmining_-_punktlichkeit_ist_eine_zier](https://media.ccc.de/v/36c3-10652-bahnmining_-_punktlichkeit_ist_eine_zier)


[^davids_error]: In the meantime, I talked to David Kriesel personally about this and showed him my evaluations. He had never looked at this aspect.




$$
9238 \cdot \frac{60 \cdot 24}{2} = 6,651,360 \space \frac{\textrm{inquiries}}{\textrm{day}}
$$


The quota of the IRIS API in the Rail API Marketplace[^6] is too limited for this. I therefore use a second, public interface[^7] of the internal traveller information system.


[^6]: Information on arrivals and departures in the form of track tables and information on a train's journey using DB Timetables API [https://developers.deutschebahn.com/db-api-marketplace/apis/product/timetables](https://developers.deutschebahn.com/db-api-marketplace/apis/product/timetables)


[^7]: I am surprised that none of the operators (DB Station & Service AG) of the API have asked me what I am doing. I had reported the procedure, but never received an answer.


Despite a lot of effort, the data of the IRIS API are poor. They are only accurate to the minute and often show that a train arrives or departs significantly too early. At least the latter does not happen in reality on the railways. I have analysed the data thoroughly in the work Jugend Forscht (Chapter 3, 2021) and base this partly on the findings from then.

\vspace{\vheader}
## Railway infrastructure data
\vspace{\vheader}

The main reason for delays is the lack of capacity on the rail network. This leads to "traffic jams". Information about train stations and the route network is therefore particularly important for predicting train delays.

\vspace{\vheader}
### Station data
\vspace{\vheader}

There are many different data sources for station data. None of these sources are complete. The IRIS has no geo-coordinates, the private collection of "derf"[^derf] with coordinates is incomplete. The Central Stop Directory (ZHV) uses different IDs than the train. The situation is similar with HAFAS, Marudor and the railway’s OpenData station set. I combine all of this data depending on availability and have also edited many stations manually. In addition, the train stations are not static, but change (name, location, existence). This seemingly simple basis requires over 300 hours of work. Because of the great effort, I publish the data set for others on my homepage [https://bahnvorhersage.de/stationviewer](https://bahnvorhersage.de/stationviewer)

[^derf]: Perl command line client for the DB IRIS departure monitor [https://github.com/derf/Travel-Status-DE-IRIS](https://github.com/derf/Travel-Status-DE -IRIS)

\vspace{\vheader}
### Note on stations, station names, EVAs[^eva] and the DS100[^ds100]
\vspace{\vheader}

Each station has a name, an EVA ID and a DS100. For queries to some APIs names are used, for others EVAs (e.g.: Hafas Routing) and for still others DS100 abbreviations (e.g.: IRIS) are used to identify the stations.

Names are not unique keys. The tram stops in Karlsruhe are often divided into two stops. Track 1 has the same name as Track 2, but different coordinates and a different EVA.

[^eva]: A kind of stop number
[^ds100]: Document 100, a list of abbreviations for train stations and operating offices of the Deutsche Bundesbahn (actually outdated, Ril100 would be new).

\vspace{\vheader}
### Route data {#sec:route_data}
\vspace{\vheader}

The distance traveled in meters is particularly crucial for the punctuality of a train. The further a train travels, the more random events can cause delays. Since the distance actually traveled is often a multiple of the distance traveled as the crow flies, a routable route network is necessary in order to be able to understand exactly how far a train has traveled.
The route network is even more complicated than the station data: In about half a year of work, I worked my way through six different data sources (OpenData portal[^geo_route_db_opendata], Trassenfinder API[^trassenfinder] [^hm], DELFI[^delfi] GTFS[^gtfs] / NeTEx[^netex], Federal Railway Office via the Freedom of Information Act[^ifg], DB networks and the infrastructure register[^isr]), only to find in the end that none of these sources are complete. Usually only the part of the route network that is operated by DB-Netze is included. Or DELFI, for example, lacked information on Saxony and Schleswig-Holstein.

[^geo_route_db_opendata]: Geoinformation on routes in the rail transport network: [data.deutschebahn.com/dataset/geo-route.html](https://data.deutschebahn.com/dataset/geo-route.html)

[^trassenfinder]: Information system for route searches on the German rail network: [trassenfinder.de](https://trassenfinder.de/)

[^hm]: Distances in hectometers (1 hm = 100 m) - a somewhat arbitrarily chosen unit in my opinion.

[^delfi]: The **D**urchgängige **el**ektronische **F**ahrplan**i**nformation (DELFI) offers Germany-wide connection information on public transport: [www.delfi.de](https://www.delfi.de/)

[^gtfs]: The **G**eneral **T**ransit **F**eed **S**specification (GTFS) (Formerly Google Transit Feed Specification) defines a digital exchange format for public transport timetables and associated geographical information. [developers.google.com/transit/gtfs](https://developers.google.com/transit/gtfs)

[^netex]: **Ne**twork and **T**imetable **Ex**change: NeTEx is the future European standard for the transmission of target data regarding the network and timetable.

[^ifg]: FragDenStaat is the central contact point for freedom of information in Germany: [fragdenstaat.de/](https://fragdenstaat.de/)

[^isr]: Infrastructure register of DB Netze [geovdbn.deutschebahn.com/isr](https://geovdbn.deutschebahn.com/isr)

That's why I create my route network from OpenStreetMaps data. These need to be converted into a routable graph. To do this, the stations must be inserted as nodes into the rail graph using a specially developed algorithm. The algorithm constructs a line cross at each station and inserts the intersections of the cross with the rails into the graph as station nodes. The rough process is the same as in Jugend Forscht (2021, Section 2.3). However, the code was completely rewritten to be able to handle larger amounts of data and more special cases. Working with geodata poses a particular challenge. Many frameworks are unfinished, often crash and are often poorly documented. The level of detail of the route network created in this way is very high: every single switch is shown. This means that I have problems loading the entire route network into memory and editing it.

\vspace{\vheader}
# Data processing
\vspace{\vheader}

The collected data on delays cannot be used in its raw form. Due to the enormous amounts of data, the processing of the data must be carefully thought out. Many processing steps that are normally trivial for a data set cannot be applied to big data.

[@fig:data_parsing] shows the rough data processing process for delay data. In this case, the data sources and the parsing of the station and route data are missing, which are significantly more complex than the entire diagram here.

![Overview of parsing](img/uebersicht_datenparsing.png){#fig:data_parsing width=100%}

Below I describe some aspects of parsing in more detail.

\vspace{\vheader}
## IDs
\vspace{\vheader}

The scheduled departure times and the changed departure times are provided separately by IRIS. In order for these to be combined with each other, each data point needs a unique ID. The IRIS uses character strings as IDs. An example ID _-406941045342705729-2009042251-1_ consists of the following partial information: _Daily reused ID of the train line_ `-` _Date_ `-` _Stop number_. For processing with Dask[^dask], integers are recommended as IDs[^dask_int_ids]. Therefore, the IRIS IDs are hashed into 64-bit integers using CityHash[^cityhash]. By the way, at least the Dask SQL module is still very susceptible to errors, even with integer IDs. With 5 integrated pull requests for Dask SQL, I am now one of the main contributors to the module. These pull requests (see @sec:opensource) all arose from acute problems downloading my data.

[^dask]: Big Data Framework for Python: [dask.org](https://www.dask.org/)

[^dask_int_ids]: Dask's SQL interface doesn't work properly with string IDs, and in general, integer IDs make Dask significantly more performant.

[^cityhash]: Non Cryptographic Hash from Google: [github.com/google/cityhash](https://github.com/google/cityhash)

\vspace{\vheader}
## Real-time parsing
\vspace{\vheader}

If possible, the data should be stored in the database, parsed in real time. A data crawler should save the data in raw form if possible. This means the data can be parsed again if an error is noticed. So data crawlers and parsers must be separate. The data crawlers store the IDs of newly collected data in a Redis Stream[^redis_stream]. Similar to the Pub/Sub architecture, the parser grabs these IDs as they are published and parses the corresponding data points.

[^redis_stream]: Append only data structure of the in-memory database Redis. [redis.io](https://redis.io/)

\vspace{\vheader}
## Routing and performance
\vspace{\vheader}

The most informative feature for predicting delays using machine learning is the length of the route already traveled. I calculate this using routing (Dijkstra) in my route network. This is very computationally expensive. Due to its size, the raw data is processed in chunks of 20,000 data points each. Routing for 20,000 data points takes 14 minutes. For all currently 470 million data points, that's around 237 days, i.e. almost a whole year. The routing itself can hardly be optimized further: I use the Igraph library written in C, and I have removed or combined all unnecessary edges and nodes in the route network graph.

Calculating the routing along the train routes in advance does not make sense, since almost every day a train is rerouted in a way that no other train has ever run before. Therefore, routing is mandatory during parsing.

To speed things up, the parser uses, among other things, caching at this point, consisting of a local cache and an external, shared cache. The route length is only actually calculated if there are two cache misses. The shared cache is in a HashMap in a Redis database. This is necessary so that different processes can parse at the same time and, thanks to the shared cache, do not have to carry out routing twice.

\vspace{\vheader}
## Parsed data
\vspace{\vheader}

The data from the IRIS after parsing is shown as an example in [@tbl:iris_data]. It is also described why I consider these features to be significant for predicting delays.

|     Train station     | Operator | Type  | number | Delay (arrival) | Delay (departure) | track | Stop  | minute |  day  | Stay time |
| :-------------------: | :------: | :---: | :----: | :-------------: | :---------------: | :---: | :---: | :----: | :---: | :-------: |
|    Bruxelles-Nord     |    88    |  IC   |  2112  |        0        |         0         |   7   |   3   |  762   |   2   |    2.0    |
|         Kaub          |    0S    |   S   | 47203  |       -1        |         0         |   2   |  18   |  589   |   1   |    0.0    |
| Friedrichshafen Stadt |  800694  |  RE   |  4217  |        0        |         0         |   4   |  13   |  863   |   6   |    5.0    |
| Nürnberg-Dutzendteich |  800721  |   S   | 39397  |        2        |         3         |   2   |   4   |   82   |   6   |    0.0    |
|     Teutschenthal     |    AM    |  Bus  | 10775  |        0        |         0         |       |   7   |  741   |   6   |    0.0    |
|     Ludwigsfelde      |  800158  |  RE   |  3305  |        1        |         1         |   1   |  28   |  600   |   5   |    1.0    |

: Parsed data from the IRIS {#tbl:iris_data}

Train station, nominal
: The name of the station. Trains in Cologne central station, for example, are delayed above average.

Operator, nominal
: The ID of the operator of the train. Different operators have different punctuality times.

Type, nominal
: The train type. Local transport is generally much more punctual than long-distance transport.

Number, nominal
: train number

Delay (arrival), Metric, Optional
: Delay in minutes for the train to arrive. Negative numbers mean that the train arrived early.

Delay (departure), Metric, Optional
: Delay in minutes for train departure.

Track, Nominal, Optional
: The track (e.g. 1, 12, or 3a-f). Delays often occur when tracks are occupied twice.

Stop, metric
: How many stops on the train is this stop? The further a train travels, the more delay it accumulates.

Minute, Metric
: The minutes from the start of the day to the scheduled arrival time. During rush hour, more trains run and there are additional delays.

Day, Metric
: Day of the week (0-6). Train events are different on weekends than on weekdays.

Stay Time, Metric, Optional
: The planned stay time in the station. A lot of waiting time is often used to catch up on delays.


In addition, there are four additional columns for the scheduled and actual train arrival time and for the scheduled and actual train departure time. These are not used directly for training the machine learning models, but are very important for many analyzes and filtering the data by date. Delays, among other things, are calculated from these columns.

By intersecting the IRIS data with the station and route data, the data can be supplemented with [@tbl:geo_data].

| Longitude | Latitude  | Distance since starting station | Distance to destination station |
| :-------: | :-------: | :-----------------------------: | :-----------------------------: |
| 50.860239 | 4.361458  |            16892.367            |            16980.846            |
| 50.083652 | 7.768262  |            902847.56            |            839542.56            |
| 47.653166 | 9.473471  |            326894.0             |            38347.37             |
| 49.436959 | 11.117391 |            5633.5557            |            11794.492            |
| 51.464817 | 11.786027 |            16029.182            |            1051.8007            |
| 52.299214 | 13.267602 |            114937.91            |            47421.707            |

: Additional geodata from the route network {#tbl:geo_data}

Longitude, interval
: Longitude of the station. The quality of the rail network varies considerably regionally.

Latitude, interval
: Latitude of the station. The quality of the rail network varies considerably regionally.

Distance from departure station, metric
: Distance traveled since the train's starting station in meters. The further a train travels, the more delay it accumulates.

Distance to destination, metric
: Distance still to travel to the train's final station in meters. The last stop is canceled more often than others.

The data from [@tbl:iris_data] and [@tbl:geo_data] are all features used for training the machine learning models.

\vspace{\vheader}
# Big Data
\vspace{\vheader}

In the project I work with big data. In other words, data that is so structured (raw data comes as XML), fast (new data comes every 2 minutes that describes what is happening on the railway in real time) and large (1.3 TB database) that I can no longer process it using conventional processing methods. Here are some examples from my project where processing big data was particularly difficult.

\vspace{\vheader}
## Data aggregation for plots
\vspace{\vheader}

In order to develop rich features for delay prediction in feature engineering, I needed, among other things, visualizations of my data. In addition, many glimpses into the data on rail events are exciting. In the work Jugend Forscht (2021, Chapter 3), I unintentionally uncovered failures at the NordBahn and analyzed the results of a snowstorm. For most analyses, I want to consider all data. This turns a one-liner for normal data into an entire module for big data.

The data must be aggregated. I do this with Dask[^dask]. Dask divides the data into blocks called partitions. For aggregation, the partitions are aggregated individually. Only the result of the calculation is kept in memory. All results are summarized again. This means you can work on several processor cores in parallel on the individual partitions. With currently around 2000 partitions, the whole thing is still slow. Therefore, I also cache the results in my PostgreSQL database.

\vspace{\vheader}
## Efficient groupbys
\vspace{\vheader}

The groupbys for data aggregation vary in speed. For the temporal and spatial overview of delays on my website ([bahnvorhersage.de/stats/stations](https://bahnvorhersage.de/stats/stations#content)) it must be known which station it is after the groupby acts. Including the train stations in the aggregation proves to be difficult, as this increases the computing time from ten minutes to over 24 hours. To get around this, I don't use the station names, but rather a label Encoding[^label_encoding] of my categorical[^categoricals] data type station.

[^label_encoding]: An integer number is assigned to each unique value.

[^categoricals]: Dask forgets the categories as soon as the data is saved. I added the workaround in the Dask documentation.

\vspace{\vheader}
# Machine learning
\vspace{\vheader}

There are many different ways to predict delays. Spanninger et al. In 2021, a meta-study analyzed 71 different publications on the forecast of train delays. You first compare the forecast models according to whether they are data-driven or event-driven. Event-driven models model the dynamics of railway operations. For my problem and my data set, I work data-driven.

The data-driven models are not based on modeling the dynamics of railway operations. Therefore, they typically produce one-step predictions. You can also predict the tenth nearest stop without having made a prediction for the stops before it. According to Spanninger et al. In 2021, most data-driven models are machine learning models. Additionally, their prediction accuracy is likely to be higher than that of event-driven models.

Spanninger et al. In 2021, the publications will be further divided according to their forecast horizon from ultra-short-term to long-term. A forecast horizon of at least four hours is defined as long-term. For my problem, that's too little and too much at the same time. Train connections are often booked weeks in advance, so forecasts need to be made weeks in advance. However, it is also possible to book a connection just a few minutes before the first train departs. Optimizing a forecasting system for these two extreme cases is very complex. In addition, there is still no research on the long-term prognosis. Wu et al. In 2021, the long-term forecasts will be examined using hybrid long short-term memory (LSTM). This process requires real-time data. Tatsui et al. 2021 uses LSTM. Oneto et al. 2018 examined train delay prediction systems from a big data perspective, with the aim of predicting the train delay of a train at its next checkpoint. This also requires real-time information. I miss this data. I don't save all the delay changes that the train reports, but only the most recent one. This means I can only train a machine learning model on the final delays and cannot therefore predict the development of delays.

I therefore focus on very long-term predictions. By this I mean predictions that can reach many days into the future. Keeping up with Deutsche Bahn's short-term forecasting system is almost impossible anyway. I don't have anywhere near the information about occupied route sections, current disruptions, etc. that the railway has available internally. Subjectively, the railway's forecasting system often seems relatively imprecise, but that is partly intentional. There is another problem when predicting train delays: it is far more problematic for passenger information if too much delay is predicted than if too little delay is predicted. If too much delay is predicted, passengers may arrive late at the station and miss their train even though they would have reached it without any problems.

\vspace{\vheader}
## Methodology
\vspace{\vheader}

An important decision is what I want to predict. The aim, as described in @sec:intro, is to evaluate the connection security of train connections. The decisive factor for the evaluation should be the accessibility of transfers. Theoretically, it would be possible to evaluate the changes directly. I decided against it because I would usefully need a data set with real train connections. This would have to contain all train connections actually purchased from the railway. But since I don't get any ticket data from the train, I can't generate this data set.

That's why I predict the delays of individual trains in order to calculate the connection security in a second step. It has proven useful not to predict discrete values using regression (a train will be exactly 5 minutes late), but rather class probabilities using multiple classification models. The classes for the predictions are defined separately for arrival ($\textrm{ar}$) and departure delays ($\textrm{dp}$):

$$
\textrm{Arrival delay} \leq n \in \{0, 1, ..., 14\} \land \textrm{Train is not canceled}
$$
$$
\textrm{Departure delay} \geq n \in \{0, 1, ..., 14\} \land \textrm{Train is not canceled}
$$

For example, below I will use $\textrm{ar}_0$ to denote the predictions of the model that predicts the class $\textrm{Arrival delay} \leq 0 \land \textrm{Train not cancelled}$.

The classes are defined differently with $\leq$ and $\geq$ because this makes it easier to calculate a connection score. More on this in [@sec:con_score].

A prediction from the machine learning models provides 30 probability values. 15 of them describe the probabilities that the train is delayed on arrival $\leq 0, 1, 2, ..., 14$ minutes (and the stop is not canceled), and 15 describe the probabilities that the train is delayed on departure $\geq 0, 1, 2, ..., 14$ minutes late (and the stop is not canceled).

\vspace{\vheader}
## Set baselines {#sec:baseline}
\vspace{\vheader}

Before predicting delays, I would like to develop a basis for when predictions are good and when they are not.

What I examine as a baseline is a general statistic of train delays. I'm not interested in how much more or less late a train is in, say, 30 minutes; I record how many trains are exactly on time, how many are canceled or are more than ten minutes late. The metric I use is the Majority Baseline. This describes how many predictions are correct if the most frequently occurring class is used as a static prediction.

Most trains are on time. About 55% of trains are exactly 0 minutes late, as shown by the blue lines in @fig:per_delay. 93% are on time according to the Deutsche Bahn definition, i.e. they are a maximum of 5 minutes late. The majority baseline for punctuality according to the definition of Deutsche Bahn is 93%. If a model correctly predicts less than 93% of on-time performance, it is useless. Since it would be more correct to assume that every train is on time. Different parts of the railway have different punctuality. In long-distance transport, only 76% punctuality is achieved. In addition, the green lines in @fig:per_delay show that trains that are already late are canceled more often than those that are slightly late.

<div id="fig:delay distribution">

![Linear scale](plots/per_delay.png){#fig:per_delay width=45%}
![Logarithmic scale](plots/per_delay_loggy.png){#fig:per_delay_loggy width=45%}

Delay distribution
</div>

All of this makes predicting delays very difficult: Since 93% of trains are on time, almost all unpunctual trains can be described as extreme values. The median delay is zero minutes. The delays are strongly skewed to the right. This skewness and the median at zero minutes make machine learning predictions very difficult. Many learning methods have problems with the unequal distribution of classes.

\vspace{\vheader}
### Data rebalancing
\vspace{\vheader}

A solution to the unbalanced data would be to balance the data through resampling[^rebalancing]. You can either use undersampling to delete as much data until the same number of trains are late and not late, or use oversampling to add as much synthetic data until the same number of trains are late and not late. Unfortunately, both methods only make sense if the imbalance in the data does not correspond to reality. I initially worked with undersampled balanced data, but the models trained on balanced data then tend to perform worse than the majority baseline on real, unrebalanced data. They simply don't correspond to "reality".

[^rebalancing]: Baptiste Rocca: ["Handling imbalanced datasets in machine learning"](https://towardsdatascience.com/handling-imbalanced-datasets-in-machine-learning-7a0e84220f28)

\vspace{\vheader}
## Feature Importance {#sec:feature_importance}
\vspace{\vheader}

Which features are used for prediction depends on the availability and information content of the data. If less informative features are used, this can increase the risk of over-fitting. The feature importance according to info gain of the features used is shown in [@fig:feature_importance]. The brighter a tile is, the higher the info gain from the feature is for a particular model.

<div id="fig:feature_importance">

![Importance of features (arrival)](plots/feature_importance_ar.png){#fig:feature_importance_ar width=45%}
![Importance of features (departure)](plots/feature_importance_dp.png){#fig:feature_importance_dp width=45%}

Info gain of the features for the delay prediction
</div>

The overall most important feature is the length of the route already traveled. This is light orange on all models. Furthermore, the waiting time at the station, the operator of the trains, the latitude of the stop and the type of train are particularly important for the prediction. [@sec:exampleday] shows that even less important features can have a very positive effect on the quality of the prediction. The accuracy of predictions on test data decreases when one of the currently used features is removed. Weather data, on the other hand, has no significant impact on the accuracy of the models and is therefore no longer used.

\vspace{\vheader}
## Machine learning model
\vspace{\vheader}

Another important step is selecting the appropriate machine learning model. In principle, any model that supports classification and can output class probabilities is suitable for the methodology I have chosen. For example: "How likely is it that a train is more than five minutes late?". Spanninger et al. In 2021, @fig:classification_of_train_delay_prediction_approaches presented which models have been used in previous research.

![Spanninger et al.: Classification of train delay prediction approaches (Data-Driven only)](plots/Spanninger%20et%20al.:%20Classification%20of%20train%20delay%20prediction%20approaches.png){#fig:classification_of_train_delay_prediction_approaches width=50%}

As shown in Table 4.1 in the 2020 paper, I had found that a Random Forest performed better on my data than a Decision Tree and extraTrees. At that time I also tested the Support Vector Machine. But this model performed so poorly that I didn't even include it in the written work. I always found neural networks very promising. I worked a lot on the architecture and training procedures for various neural networks. However, the predictions of the neural networks were mostly wrong. This is probably because the neural networks cannot be trained well with the unbalanced data. The random forest, for example, is much more suitable here. In 2021 I swapped the random forest for an XGBoost model because it performed significantly better. The accuracy of the model $\textrm{ar}_0$ with XGBoost is about 10% better than a random forest.

For this I performed hyperparameter tuning with the Optuna[^optuna] framework. (Time required approx. 40 hours, calculation time approx. 1.5 weeks)

[^optuna]: Hyperparameter optimization framework: [https://optuna.org/](https://optuna.org/)

\vspace{\vheader}
## Autotraining of machine learning models
\vspace{\vheader}

The models should always be trained on the data from the last six weeks. This means the model automatically adapts to the season and the current traffic situation. Long-term construction measures are also captured by the model, provided they are reflected in the data. For predictions to be of high quality, the forecast models must be as up-to-date as possible.

In order for the models to be trained, all current data must be parsed at the time of training. For this reason, all data is parsed in real time. The need for the complex structure of the parser comes, among other things, from the fact that I want to keep the project available and up-to-date on my website for everyone.

The training itself is actually relatively simple. A module already developed in my project downloads the data from the database and saves it in Parquet format. The data is then later reloaded, filtered and reprocessed via this module so that the machine learner can be trained on it.

Another problem arose here: All of my programs run on my server packed in containers in a Kubernetes cluster, which at the time did not support a GPU by default. However, the GPU is urgently needed for training the machine learning models, otherwise the training process is very slow. The installation of GPU support was still in beta at the time and was very time-consuming for me due to many different errors.

\vspace{\vheader}
## Connection score {#sec:con_score}
\vspace{\vheader}

The connection score is the final result of my delay forecast, which is displayed on my website. It describes the probability of getting a connection.

I assume that two minutes of real transfer time are needed to reach the connection[^tra_time]. The sum of the probabilities of all cases in which 2 minutes of transfer time remain results in the connection score. For example, if an arriving train is five minutes late with a five-minute transfer time, the departing train must leave at least two minutes late so that two minutes of transfer time remains.

[^tra_time]: This assumption is often wrong. There is no database for better assumptions.

\vspace{\vheader}
# Accuracy of predictions
\vspace{\vheader}

## Accuracy of delay predictions {#sec:accuracy}
\vspace{\vheader}

Predicting train delays is anything but easy. As described in @sec:baseline, the majority baselines for many predictions are well above 90%. In order to evaluate the models, I also take into account that the models are retrained every day. So I not only want to evaluate my 30 different models, but also how their accuracy changes over time. I rate the models based on how well they perform in a “real” scenario. To do this, I chose one date as the train-test split. I name a model by the train-test split date. The models are trained on data from the six weeks before that date and tested on data from the day after that date.

In @fig:accuracy the majority baselines and the accuracy of all 30 models used are for the period from May 1st. shown until September 1, 2022. For each day, the models were trained and evaluated using the respective day as a train-test split.

<div id="fig:accuracy">

![](plots/stats_ar_0_6.png){#fig:stats_ar_0_6 width=45%}
![](plots/stats_dp_0_6.png){#fig:stats_dp_0_6 width=45%}

![](plots/stats_ar_6_12.png){#fig:stats_ar_6_12 width=45%}
![](plots/stats_dp_6_12.png){#fig:stats_dp_6_12 width=45%}

![](plots/stats_ar_12_15.png){#fig:stats_ar_12_15 width=45%}
![](plots/stats_dp_12_15.png){#fig:stats_dp_12_15 width=45%}

Accuracy of delay predictions
</div>

In @fig:stats_ar_0_6 you can see from the built line (dashed) that the majority baseline for the $\textrm{ar}_0$ is a good 50%. This means that a good 50% of trains are delayed by exactly zero minutes. The blue line shows how good the $\textrm{ar}_0$ predictions are. The predictions are almost 80% correct. My model can predict whether a train will be exactly on time with almost 80% accuracy. This is very good compared to the majority baseline. The baseline (orange, dashed) for the $\textrm{ar}_1$ is significantly higher, at an average of almost 70%. The prediction of the $\textrm{ar}_1$ (orange) is still significantly better than the baseline, but not nearly as impressive as the $\textrm{ar}_0$. The higher the predicted delay of the models, the higher the baseline, the smaller the improvement in prediction compared to the baseline. There is hardly any difference between the 12, 13 and 14 minute models in @fig:stats_ar_12_15 and @fig:stats_dp_12_15. The specific values of the prediction are only marginally above the baseline. The $\textrm{dp}_0$ in @fig:stats_dp_0_6 is out of line. This is because this is as described in [@sec:con_score].
$$
\textrm{Departure delay} \geq 0 \land \textrm{Train is not canceled}
$$
is defined. It therefore effectively only predicts the failures. Since the models for high delay predictions generally do not really perform better than the baseline, I only use models for up to 14 minutes of prediction. In 2021 I used 40 models each.

Compared to the work in 2021, I was able to significantly increase the accuracy of the predictions. The $\textrm{ar}_0$ model still had an accuracy of almost 70% in 2021. By improving the hyperparameters, training period and data quality, I now achieve 80% accuracy. The models for higher delays have only improved marginally, as they are only just above the baseline anyway.

\vspace{\vheader}
## Accuracy of delay predictions on an example day {#sec:exampleday}
\vspace{\vheader}

The results of [@sec:accuracy] show that the predictions are good. In order to further strengthen trust in the models, it would be useful to show how individual features affect the predictions. In [@fig:over_day] the punctuality[^punctuality] of all trains on September 1st, 2022 is shown. The red and orange lines show that punctuality drops particularly at peak times. Trains are also more punctual in the morning than in the evening.

[^punctuality]: Maximum of five minutes late

<div id="fig:predictions_over_day">

![Punctuality on September 1st, 2022](plots/over_day_on_time_2022_09_01.png){#fig:over_day width=45%}
![Punctuality forecast for September 1st, 2022](plots/over_day_predictions_2022_09_01.png){#fig:over_day_predictions width=45%}

Predictions compared to actual daily progress
</div>

[@fig:over_day_predictions] shows the forecast punctuality for September 1st, 2022. Predicted punctuality is very similar to actual punctuality. The machine learning model has obviously recognized the connection between the time of day and punctuality. And this despite the fact that, according to [@sec:feature_importance], the minute since the start of the day does not have a particularly big influence on punctuality. The fact that [@fig:over_day_predictions] and [@sec:feature_importance] are not identical means that the predictions are not 100% accurate.

\vspace{\vheader}
## Connection score accuracy
\vspace{\vheader}

The accuracy of the connection score is crucial for the quality of the predictions on my website. I divide the connection score analysis into two parts. The data in the following figures is from 9/1/2022. The predictions come from models based on data from the last six weeks before September 1st. were trained.

For the analysis, I extracted possible transfer connections from the data. To do this, I sorted the data by train station and then searched the train stations for possible connections with a transfer time of two to ten minutes[^note_connecting_trains].

[^note_connecting_trains]: What sounds trivial is incredibly complicated given the large amounts of data. For example, sorting by train station must be done in individual queries by train station, otherwise all data would have to be loaded into the main memory at once, which is never an option with big data.

\vspace{\vheader}
## Five minutes transfer time
\vspace{\vheader}

I would like to evaluate the connection score independently of the transfer time. I would like to answer the question of whether the connection score is able to sort connections with the same transfer time according to their quality. @fig:con_score_5_min_planned_transfer_time shows that all connections have a planned transfer time of 5 minutes.

<div id="fig:con_score_5_min">

![Five min planned transfer time](plots/con_score_5_min_planned_transfer_time.png){#fig:con_score_5_min_planned_transfer_time width=45%}
![Actual transfer time](plots/con_score_5_min_real_transfer_time.png){#fig:con_score_5_min_real_transfer_time width=45%}

Connection score with a five minute transfer time
</div>

The red trend line in @fig:con_score_5_min_real_transfer_time has a positive slope, so my AI's prediction also provides information gain that the users of my website can use, regardless of the transfer time. Furthermore, the Spearman rank correlation coefficient is $0.102$. The P-value of the correlation test is $3 \cdot 10^{-15}$. It follows that the correlation is significant with high certainty probability. The observed correlation is therefore not random. With a connection score of 90 to 100%, 95% of the connections are achieved. However, if it is between 60 and 70%, only 85% of the connections work. The connection score is therefore not a probability that a connection will work, but tends to be higher for better connections and lower for worse connections.

As always, there are extreme outliers. These are cases where the delays are not at all predictable.

\vspace{\vheader}
## Two to ten minutes transfer time
\vspace{\vheader}

On my website you can easily see which connections are secure and how secure. The transfer time plays a big role. The longer the transfer time, the more likely it is that the connection will be reached. However, it is difficult to estimate how much one or two minutes more transfer time will make. My forecasts also provide an overview here. I am therefore also investigating how the connection score behaves with a transfer time of two to ten minutes.

<div id="fig:con_score_2_to_10_min">

![Two to ten minutes planned transfer time](plots/con_score_2_to_10_min_planned_transfer_time.png){#fig:con_score_2_to_10_min_planned_transfer_time width=45%}
![Actual transfer time](plots/con_score_2_to_10_min_real_transfer_time.png){#fig:con_score_2_to_10_min_real_transfer_time width=45%}

Connection score with a transfer time of two to ten minutes
</div>

In @fig:con_score_2_to_10_min_planned_transfer_time it can be seen that connections with more transfer time are generally rated better. If you compare these with the actual transfer times in @fig:con_score_2_to_10_min_real_transfer_time, you can see that the red trend line is even steeper when the actual transfer time is taken into account. Even in this more realistic example, my forecast provides additional information.

<!-- 5 min connections con_score stats:
con_score between 0.0 and 0.1: 48.48% (33 data points)
con_score between 0.1 and 0.2: 53.61% (166 data points)
con_score between 0.2 and 0.3: 66.28% (341 data points)
con_score between 0.3 and 0.4: 79.52% (664 data points)
con_score between 0.4 and 0.5: 78.90% (967 data points)
con_score between 0.5 and 0.6: 81.57% (2160 data points)
con_score between 0.6 and 0.7: 86.74% (4503 data points)
con_score between 0.7 and 0.8: 91.30% (8836 data points)
con_score between 0.8 and 0.9: 93.71% (19847 data points)
con_score between 0.9 and 1.0: 95.63% (21083 data points)
2 - 8 min connections con_score stats:
con_score between 0.0 and 0.1: 31.03% (58 data points)
con_score between 0.1 and 0.2: 41.56% (231 data points)
con_score between 0.2 and 0.3: 46.32% (516 data points)
con_score between 0.3 and 0.4: 62.54% (961 data points)
con_score between 0.4 and 0.5: 69.63% (1643 data points)
con_score between 0.5 and 0.6: 76.30% (2953 data points)
con_score between 0.6 and 0.7: 83.45% (5062 data points)
con_score between 0.7 and 0.8: 89.70% (7699 data points)
con_score between 0.8 and 0.9: 93.78% (14043 data points)
con_score between 0.9 and 1.0: 96.16% (25433 data points) -->

\vspace{\vheader}
# Website and infrastructure
\vspace{\vheader}

## Infrastructure
\vspace{\vheader}

My website is running productively. I own and maintain the hardware myself for cost reasons. My problems require a lot of CPU, RAM, SSD and GPU. That's why I run my own server with a 32-core processor, 160 GB of RAM, 2 TB SSD (RAID 0) and an NVIDIA 3080 graphics card.

\vspace{\vheader}
## Tech Stack
\vspace{\vheader}

For performance reasons, the Postgres database runs bare metal on the server, everything else is deployed in a Kubernetes cluster. All processes running on the server are shown in [@fig:tech_stack].

![Tech Stack](img/tech_stack.png){#fig:tech_stack width=75%}

\vspace{\vheader}
## Website
\vspace{\vheader}

The development of the website with VueJS and Python Flask accounts for a third of the total project effort and receives around 100 hits every day.

\vspace{\vheader}
# Open source contributions {#sec:opensource}
\vspace{\vheader}

Since my project has relatively specific requirements, I had to add missing functions in some open source frameworks myself. Here is a list of my posts:

\vspace{\vheader}
## Dask
\vspace{\vheader}

- [Remove sqlachemy 1.3 compatibility](https://github.com/dask/dask/pull/9695)
- [Moving to SQLAlchemy >= 1.4](https://github.com/dask/dask/pull/8158)
- [fix index_col duplication if index_col is type str](https://github.com/dask/dask/pull/7661)
- [dd.from_sql_table() duplicates index with SQLAlchemy==1.4.1](https://github.com/dask/dask/issues/7436)
- [Example for working with categoricals and parquet](https://github.com/dask/dask/pull/7085)
- [Added bytes/row calculation when using meta](https://github.com/dask/dask/pull/6585)
- [dd.read_sql_table could calculate npartitions even if meta is defined](https://github.com/dask/dask/issues/6569)

\vspace{\vheader}
## OSMnx
\vspace{\vheader}

- [Added note to warn against #791](https://github.com/gboeing/osmnx/pull/803)
- [Loading large areas won't necessarily load the whole area](https://github.com/gboeing/osmnx/issues/791)

\vspace{\vheader}
## Matplotlib
\vspace{\vheader}

- [Added example on how to make packed bubble charts](https://github.com/matplotlib/matplotlib/pull/18223)

\vspace{\vheader}
# Summary
\vspace{\vheader}
In 3.5 years of work, I have collected what is probably the largest German non-rail delay data set. In many iterations, I developed a machine learning system, checked and improved it again and again in order to predict future delays. These are used to find train connections that are as secure as possible. All of this is easy, can be used around the clock and on any device via my website [bahnvorhersage.de](https://bahnvorhersage.de/).

\vspace{\vheader}
# Outlook
\vspace{\vheader}
In the future, we would like to use the predictions to develop our own routing system based on gtfs timetable data for the whole of Germany. Users should be suggested alternative connections to poor connections when booking. The ratings will include how good or bad the alternative connections are. If another train leaves 15 minutes after a missed connection, this is not serious. In addition, in some individual cases it is possible to find connections that should not work according to the timetable because without delays there would not be enough transfer time. This makes it possible to find train connections that are faster than would actually be possible.

\newpage{}

\vspace{\vheader}
# Literatur
\vspace{\vheader}

Döllmann, Meurers, TrAIn_Connectione_Prediction (Jugend Forscht 2021). Available at: [https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202021.pdf](https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202021.pdf)

Döllmann, Meurers, TrAIn_Connectione_Prediction (Jugend Forscht 2020). Available at: [https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202020.pdf](https://gitlab.com/bahnvorhersage/docs/-/blob/main/Old%20Docs/JuFo%202020.pdf)

Spanninger, Thomas and Trivella, Alessio and Büchel, Beda and Corman, Francesco, A Review of Train Delay Prediction Approaches (November 16, 2021). Available at SSRN: [http://dx.doi.org/10.2139/ssrn.3964737](http://dx.doi.org/10.2139/ssrn.3964737)

Wu, J.; Du, B.; Wu, Q.; Shen, J.; Zhou, L.; Cai, C.; Zhai, Y.; Wei, W.; Zhou, Q. A Hybrid LSTM-CPS Approach for Long-Term Prediction of Train Delays in Multivariate Time Series. Future Transp. 2021, 1, 765–776. [https://doi.org/10.3390/futuretransp1030042](https://doi.org/10.3390/futuretransp1030042)

TATSUI, Daisuke & NAKABASAMI, Kosuke & KUNIMATSU, Taketoshi & SAKAGUCHI, Takashi & TANAKA, Shunichi. (2021). Prediction Method of Train Delay Using Deep Learning Technique. Quarterly Report of RTRI. 62. 263-268. 10.2219/rtriqr.62.4_263. 

Luca Oneto, Emanuele Fumeo, Giorgio Clerico, Renzo Canepa, Federico Papa, Carlo Dambra, Nadia Mazzino, Davide Anguita, Train Delay Prediction Systems: A Big Data Analytics Perspective, Big Data Research, Volume 11, 2018, Pages 54-64, ISSN 2214-5796, [https://doi.org/10.1016/j.bdr.2017.05.002](https://doi.org/10.1016/j.bdr.2017.05.002).

\vspace{\vheader}
# Support
\vspace{\vheader}

First and foremost, I would like to thank Marius De Kuthy Meurers, with whom I worked on this project for a long time.

Many thanks to everyone who supported me in my work.

- **Long version**: Many thanks to everyone who proofread the long version and gave me tips.
- **Uni Tübingen / MPI for Intelligent Systems / BWKI**: Provision of computing power using CPUs and GPUs.
- **DB Analytics**: Insights into rail operations and answers to internal rail questions. In addition, my contact person Jochen Völker wins the Low Latency Award (latency ≤ 10min) when answering emails.
- **OpenStreetMaps**: Without a lot of people who have created and are constantly renewing and improving a public and extremely accurate map of the German rail network, I would probably still not have a routable route network.
- **SFZ Eningen**: My server is currently in the SFZ Eningen. Many thanks to the SFZ, which has covered all electricity costs so far.
- Many thanks to **Mr. Glück**, who gave me a lot of support in purchasing my own "supercomputer". I received financial support for this from:
     - Bosch
     - Mr. Mörike
     - Kepi Federal District
     - Micron